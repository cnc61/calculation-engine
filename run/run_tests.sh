#!/bin/env bash
set -e

cd ${CE_BASE_DIR}/app

bash ../run/wait-for-it.sh ${API_SERVER_HOST}:${API_SERVER_PORT} --timeout=0

python manage.py test -v2 calculation_engine.tests.test_api
python manage.py test -v2 calculation_engine.tests.test_config
python manage.py test -v2 calculation_engine.tests.test_saved_jobs
python manage.py test -v2 calculation_engine.tests.test_uploads
python manage.py test -v2 calculation_engine.tests.test_error_messages
# The workflow tests are expensive. Run them manually.
# python manage.py test -v2 calculation_engine.tests.test_workflows
