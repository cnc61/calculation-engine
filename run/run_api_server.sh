#!/bin/env bash
set -e

cd ${CE_BASE_DIR}/app


INIT_STARTED_DB="${CE_BASE_DIR}/static/.initializing_db"
if [[ "${FORCE_INITIALIZATION}" == "true" ]]; then
  rm -f "${INIT_STARTED_DB}"
fi

## Initialize Django database and static files
##
bash ../run/wait-for-it.sh ${DATABASE_HOST}:${DATABASE_PORT} --timeout=0

INIT_LOOP=1
while [ $INIT_LOOP == 1 ]; do
    if [[ -f "${INIT_STARTED_DB}" ]]
    then
        echo "Django database and static files are currently being initialized (\"${INIT_STARTED_DB}\" exists)."
        sleep 10
    else
        INIT_LOOP=0
        echo "\"${INIT_STARTED_DB}\" not found. Running initialization script..."
        touch "${INIT_STARTED_DB}"

        bash ../run/django_init.sh

        rm -f "${INIT_STARTED_DB}"
        echo "Django database initialization complete."
    fi
done

# ## Process config file. Run initialization script via Django manage.py so Django settings are available.
# python manage.py shell --command='from calculation_engine.initialize import main; main();'

# Run tests if in test mode and exit
if [[ $TEST_MODE == "true" ]]; then
    bash ../run/run_tests.sh
    exit 0
fi

# Start server
if [[ $DEV_MODE == "true" ]]; then
    python manage.py runserver 0.0.0.0:${API_SERVER_PORT}
else
    ## Run Django server via Gunicorn
    ## see https://docs.djangoproject.com/en/5.0/howto/deployment/asgi/uvicorn/
    uvicorn \
    --host=0.0.0.0 \
    --port=${API_SERVER_PORT} \
    --workers=2 \
    --log-level=info \
    app_base.asgi:application
fi
