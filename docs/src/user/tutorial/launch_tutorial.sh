#!/bin/bash

set -e

cd "$(dirname "$(readlink -f "$0")")"

# Install the CE API wrapper Python module
cp ../../../../app/calculation_engine/tests/calculation_engine_api.py .

docker run --rm --name jupyterlab --hostname jupyter \
    --user "$(id -u)" --group-add users \
    -p 8888:8888 \
    -e CHOWN_HOME=yes \
    -e CHOWN_HOME_OPTS="-R" \
    -v $(pwd):/home/jovyan/work \
    quay.io/jupyter/scipy-notebook:latest \
    jupyter notebook \
    --NotebookApp.token=''
