MUSES Calculation Engine Tutorial
=================================

The tutorial is a Jupyter notebook that allows you to interactively run
commands to learn how to use the Calculation Engine. There are `many
ways to run a Jupyter notebook <https://jupyter.org/>`__. Here we offer two methods.

🛑 **STOP!** Before you proceed to the tutorial, please follow the steps in the 
:doc:`/user/quickstart` guide to ensure that you have a MUSES account
and that you are logged into
`the Calculation Engine <https://ce.musesframework.io>`__.


MUSES JupyterHub
----------------

The first method uses our `MUSES JupyterHub instance here <https://jupyter.musesframework.io/>`__. 
When your personal JupyterLab server is online, you should open a new Terminal with **File -> New -> Terminal**.
Copy the following commands into the terminal to download and install the tutorial files:

.. code:: bash

   #!/bin/bash
   set -e
   wget -O /home/jovyan/tutorial.ipynb --quiet \
       https://gitlab.com/nsf-muses/calculation-engine/-/raw/main/docs/src/user/tutorial/tutorial.ipynb
   wget -O /home/jovyan/calculation_engine_api.py --quiet \
       https://gitlab.com/nsf-muses/calculation-engine/-/raw/main/app/calculation_engine/tests/calculation_engine_api.py

You should now see a :code:`tutorial.ipynb` file in your home folder. Open that notebook to begin the tutorial.

Local JupyterLab server
-----------------------

The second method requires `Docker to be installed <https://docs.docker.com/engine/install/>`__ on your machine.

1. `Download the source code
   here <https://gitlab.com/nsf-muses/calculation-engine/-/archive/main/calculation-engine-main.zip>`__
   and unzip the files.
\

1. Navigate into the ``/docs/src/user/tutorial`` directory, and from there run the
   script to launch a local JupyterLab server:

   .. code:: bash

      $ bash launch_tutorial.sh

        [I ... ServerApp] Jupyter Server 2.14.2 is running at:
           ...
        [I ... ServerApp] Use Control-C to stop this server and shut down all kernels...

   Once the Docker image has finished downloading and the container is
   online, open the notebook in your browser at
   http://127.0.0.1:8888/lab/tree/work/tutorial.ipynb.
