# Calculation Engine (CE) Terms of Service

This document defines the Calculation Engine (CE) Terms of Service that all users agree to be bound by when using the system.

## Alpha release

**All versions of the CE prior to v1.0.0 are referred to as the "alpha release"**. The alpha release is only for use by a limited set of specifically invited users called "alpha testers". If you are one of these invited users you must agree to the general CE terms of service in addition to the terms in this section.

You understand that the alpha release may still contain bugs as we undergo testing. Thus, drawing physical conclusions from any of these modules may be misleading and not representative of the underlying physical mechanisms.

**You agree not to publish a paper about a given module or results obtained via the Calculation Engine without our explicit permission** until notified otherwise. There are still MUSES collaboration members who have not yet published papers about their software and associated scientific results. We will update the MUSES documentation when it becomes possible to release your own data based on data generated by the Calculation Engine and/or MUSES module software (including the source code, implemented algorithms, or output of execution). *We anticipate that all code will be open-source and usable in publications by the beta release.*

## Fair use policy

As a user of the Calculation Engine hosted by the MUSES collaboration, you agree to a policy of fair use in order to ensure that all researchers in the scientific community are able to run their workflows in productive way. In practice this means that [your individual usage limits](https://ce.musesframework.io/ce/limits/) may change depending on the system load, and in rare cases we may contact you directly to ask you to change your usage if it is adversely impacting other users.

## Crediting MUSES software

When using data generated by the MUSES Calculation Engine, we ask that you cite all necessary modules and the MUSES Calculation Engine itself. This is to ensure both proper credit to the scientists who developed the software but also to ensure reproducibility of the data. See [this page to download citation information](citation.html).

## Reporting

Bugs and software issues can be discussed with the MUSES developers on [🛟 the support forum](https://forum.musesframework.io/). Users may be directed to  create issues in the relevant source code repo ([most are within this GitLab group](https://gitlab.com/nsf-muses)).

Sensitive questions such as those related to security vulnerabilities may be [✉️ sent directly to a the MUSES leadership using this link](https://forum.musesframework.io/new-message?groupname=leadership&title=Calculation%20Engine%20issue&body=Please%20describe%20your%20concern).

## Security

You are responsible for all activity related to your authenticated account. 

Your account may not be shared with other people. You may not share your API token with others. There are no end-user "service accounts".
