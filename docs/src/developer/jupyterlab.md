# JupyterLab

To launch the CE locally with an integrated JupyterLab instance, use the `jupyter` profile when launching the application:

```bash
bash docker/launch.sh jupyter up
```

By default you should be able to access the tutorial notebook by opening http://127.0.0.1:8888/lab/tree/work/tutorial.ipynb in your browser.
