ArgoCD
============================

This [excellent tutorial](https://www.arthurkoziel.com/setting-up-argocd-with-helm/) is what we followed to deploy ArgoCD. The private repo access was provided manually via the ArgoCD web UI by first generating a `repo_read` API token on GitLab and using it as the password in the Repository settings.

Authorization
-----------------------------

[Members of the GitLab group `nsf-muses`](https://gitlab.com/nsf-muses/deployment/kubernetes/-/blob/db1d9b0e64d20814f812a61b069631b23b19adfc/charts/argo-cd/values.yaml#L61-72) can use the **Log in via GitLab** button to authenticate to the [ArgoCD web UI](https://musesframework.io/argo-cd/) and control the MUSES Kubernetes cluster deployments.

This authentication via GitLab relies on [the GitLab Application accessible in the settings here](https://gitlab.com/groups/nsf-muses/-/settings/applications). This is under control of the `nsf-muses` group instead of an individual admin's account.

Command line access
-----------------------------

To use the local [ArgoCD CLI](https://argoproj.github.io/argo-cd/getting_started/#2-download-argo-cd-cli), use the more complex login command, which will open a browser window to complete the GitHub authentication:

```bash
argocd login musesframework.io --grpc-web-root-path /argo-cd --sso
```

Installation
-----------------------------

Install ArgoCD manually via Helm:

```bash
helm upgrade --install -n argo-cd argo-cd charts/argo-cd
```

Optionally, add the ArgoCD deployment as an app so ArgoCD can manage itself. Once synced via the ArgoCD web UI, remove the Helm release secret:

```bash
$ kubectl delete -n argo-cd secret -l owner=helm,name=argo-cd
secret "sh.helm.release.v1.argo-cd.v1" deleted
```
