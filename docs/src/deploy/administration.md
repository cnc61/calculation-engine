# Administration and Access

The administrator of the MUSES services is [Andrew Manning](https://forum.musesframework.io/u/andrew.manning/summary).

Ultimately, complete root-level access to the Kubernetes cluster nodes and deployed resources stems from permissions granted by the [NCSA Rancher instance](https://gonzo-rancher.ncsa.illinois.edu/) that is configured to manage the cluster (and which also initially bootstrapped the cluster). Once a person has been authorized to log in to the Rancher instance, and they have been added to the Cluster Members for the target cluster, they can download the kubeconfig.

Management of application deployment is handled by [our ArgoCD instance](https://musesframework.io/argo-cd/). While admin access to ArgoCD can be achieved via `kubectl`, using `kubectl port-forward` and access to the relevant Kubernetes Secret, to authenticate with the ArgoCD local `admin` credentials, one can also log in using the GitLab authentication button. This method requires membership in the `nsf-muses` [GitLab group](https://gitlab.com/groups/nsf-muses/-/group_members). Our GitOps-based deployment repo is also hosted on GitLab within this group. Thus, GitLab provides the authentication and authorization for both the git repo and ArgoCD instance that together drive the deployment of services.
