+------------------+
| |"MUSES banner"| |
+------------------+

MUSES Documentation
=========================================================

`MUSES (Modular Unified Solver of the Equation of State) <https://musesframework.io>`__ is an NSF-funded large collaboration project that is developing cyberinfrastructure to provide the scientific community novel tools to answer critical interdisciplinary questions in nuclear physics, gravitational wave astrophysics, and heavy-ion physics.

.. tip::
   If you are a researcher who wants run scientific workflows, we recommend starting with the :doc:`/user/quickstart`.

This is the top-level documentation for our project. It includes sections targeting several audiences including:

- researchers who want to use the Calculation Engine to run scientific workflows
- scientists who want to adapt their own calculation software to the MUSES framework for integration with the Calculation Engine
- `research software engineers <https://us-rse.org/>`__ who want to learn how to develop the Calculation Engine and/or deploy it on their own hardware
- system administrators who operate and maintain the MUSES cyberinfrastructure

.. toctree::
   :maxdepth: 1
   :caption: User Manual
   :glob:

   user/quickstart
   user/tutorial/*
   user/workflows
   user/policies/*

.. toctree::
   :maxdepth: 1
   :caption: Module Documentation
   :glob:

   modules/*/*

.. toctree::
   :maxdepth: 1
   :caption: Developer Guide
   :glob:

   developer/overview
   developer/module
   developer/*

.. toctree::
   :maxdepth: 1
   :caption: Infrastructure and Deployment
   :glob:

   deploy/overview
   deploy/administration
   deploy/storage
   deploy/sealed-secrets
   deploy/**


.. |"MUSES banner"| image:: _static/img/Muses_Full_Color_banner.png
