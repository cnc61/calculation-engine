from django.db import models
import uuid
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User
import hashlib

from calculation_engine.log import get_logger
logger = get_logger(__name__)


class Job(models.Model):
    class Meta:
        # -created means newest first
        # to avoid DRF pagination UnorderedObjectListWarning
        ordering = ['-created']
        # db_table = 'ce_jobs'
        verbose_name = _('job')
        verbose_name_plural = _('jobs')

    class JobStatus(models.TextChoices):
        '''
        Django docs: https://docs.djangoproject.com/en/5.0/ref/models/fields/#enumeration-types
        '''

        # Use the built-in Celery states
        PENDING = 'PENDING', _('Pending')
        STARTED = 'STARTED', _('Started')
        SUCCESS = 'SUCCESS', _('Success')
        FAILURE = 'FAILURE', _('Failure')
        RETRY = 'RETRY', _('Retry')
        REVOKED = 'REVOKED', _('Revoked')

    name = models.TextField(blank=True)
    description = models.TextField(blank=True)
    owner = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    error_info = models.TextField(blank=True)
    config = models.JSONField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Time Created')
    modified = models.DateTimeField(auto_now=True, verbose_name='Last Modified')
    uuid = models.UUIDField(
        default=uuid.uuid4,
        unique=True,
        db_index=True,
        primary_key=True
    )
    status = models.CharField(
        max_length=10,
        choices=JobStatus.choices,
        default=JobStatus.PENDING,
    )
    saved = models.BooleanField(default=False, null=False, blank=True)
    public = models.BooleanField(default=False, null=False, blank=True)
    task_ids = models.JSONField(null=True, blank=True)
    current_processes = models.JSONField(default=list, blank=True)
    completed_processes = models.JSONField(default=list, blank=True)

    def __str__(self):
        ret = f'job: {self.uuid}, owner: {self.owner}, status: {self.status}'
        ret += f', created: {self.created}, config: {self.config}'
        return ret


class Metric(models.Model):
    time_collected = models.DateTimeField(auto_now_add=True, verbose_name='Time Collected')
    jobs_total = models.IntegerField(null=False, blank=False)
    jobs_success = models.IntegerField(null=False, blank=False)
    jobs_failure = models.IntegerField(null=False, blank=False)
    users_count = models.IntegerField(null=False, blank=False)
    users_active = models.IntegerField(null=False, blank=False)
    uploads_total = models.IntegerField(null=False, blank=False)
    uploads_size = models.BigIntegerField(null=False, blank=False)
    job_files_total = models.IntegerField(null=False, blank=False)
    job_files_size = models.BigIntegerField(null=False, blank=False)
    module_usage = models.JSONField(null=False, blank=False, default=dict)


def upload_dir_path(instance, filename):
    upload_path = f'''{instance.uuid}/{instance.path}'''
    # Calculate checksum
    data = instance.file.read()
    checksum = hashlib.md5(data).hexdigest()
    logger.debug(f'''"{upload_path}": {checksum}''')
    instance.checksum = checksum
    # Return upload path
    return upload_path


class Upload(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True, db_index=True, primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    path = models.CharField(max_length=None, default='')
    description = models.TextField(blank=True)
    file = models.FileField(upload_to=upload_dir_path, null=False, blank=False)
    owner = models.ForeignKey(User, null=False, blank=False, on_delete=models.CASCADE)
    public = models.BooleanField(default=False, null=False, blank=True)
    size = models.BigIntegerField(null=False, blank=False, default=0)  # Size of the stored file in bytes
    checksum = models.CharField(max_length=32, blank=True, null=False, default='')

    def __str__(self):
        ret = f'path: {self.path}, description: {self.description}, public: {self.public}'
        ret += f', created: {self.created}'
        return ret


class JobFile(models.Model):
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    path = models.CharField(max_length=None, default='')
    size = models.BigIntegerField(null=False, blank=False, default=0) # Size of the stored file in bytes


def update_workflow_state(job_id, process_names=None, status=None):
    assert status in ['active', 'completed']
    assert process_names
    job = Job.objects.get(uuid__exact=job_id)
    # Append to list of active processes if not already in list
    if status == 'active':
        job.current_processes.extend([proc_name for proc_name in process_names
                                      if proc_name not in job.current_processes])
    if status == 'completed':
        # Remove from list of active processes
        for proc_name in process_names:
            try:
                job.current_processes.remove(proc_name)
            except Exception as err:
                logger.error(f'Ignoring error while removing process from current job processes: {err}')
        # Append to list of completed processes
        job.completed_processes.extend([proc_name for proc_name in process_names
                                        if proc_name not in job.completed_processes])
    job.save()


def update_job_state(job_id, state, error_info=''):
    logger.debug(f'''Updating job "{job_id}" state to: "{state}"...''')
    job = Job.objects.get(uuid__exact=job_id)
    job.status = state
    job.error_info = error_info
    # Forcibly clear current processes list
    if state in [Job.JobStatus.SUCCESS, Job.JobStatus.FAILURE]:
        job.current_processes = []
    job.save()
