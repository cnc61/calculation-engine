from calculation_engine.models import Job, JobFile, Upload, Metric
from rest_framework import serializers
from calculation_engine.log import get_logger

logger = get_logger(__name__)


class JobSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Job
        read_only_fields = ['owner', 'created', 'uuid', 'error_info', 'status', 'modified', 'files', 'task_ids',
                            'current_processes', 'completed_processes']
        fields = read_only_fields + ['name', 'description', 'files', 'saved', 'public', 'config']
    files = serializers.SerializerMethodField()

    def get_files(self, job):
        jobfiles = JobFile.objects.filter(job__exact=job)
        return [{'path': jobfile.path, 'size': jobfile.size} for jobfile in jobfiles]


class UploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Upload
        fields = ('file', 'created', 'path',
                  'owner', 'public', 'description', 'uuid', 'size', 'checksum')
        read_only_fields = ['owner', 'created', 'uuid', 'size']


class MetricSerializer(serializers.ModelSerializer):
    class Meta:
        model = Metric
        fields = (
            'time_collected',
            'jobs_total',
            'jobs_success',
            'jobs_failure',
            'users_count',
            'users_active',
            'uploads_total',
            'uploads_size',
            'job_files_total',
            'job_files_size',
            'module_usage',
        )
        read_only_fields = [
            'time_collected',
            'jobs_total',
            'jobs_success',
            'jobs_failure',
            'users_count',
            'users_active',
            'uploads_total',
            'uploads_size',
            'job_files_total',
            'job_files_size',
            'module_usage',
        ]
