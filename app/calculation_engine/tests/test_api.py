from django.test import TestCase, RequestFactory
from calculation_engine.views import ModuleListView


class CalculationEngineApiTests(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_list_module(self):
        # print('Testing list modules...')
        request = self.factory.get("/api/v0/ce/module", headers={
            'Content-Type': 'application/json',
        })
        response = ModuleListView(request)
        self.assertEqual(response.status_code, 200)
        # The data object should be a list
        self.assertIsInstance(response.data, list)
