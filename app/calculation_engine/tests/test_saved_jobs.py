from django.test import TestCase
from calculation_engine.tests.utils import purge_and_create_users, set_env_vars, get_staff_apis
from calculation_engine.limits import get_usage_limits
import yaml
import time


def get_workflow_config(idx):
    wf_configs = ['''
        processes:
          - name: cmf
            module: cmf_solver
            config:
              variables:
                chemical_optical_potentials:
                  muB_begin: 1000.0
                  muB_end: 1400.0
                  muB_step: 200.0
                  muQ_begin: -300.0
                  muQ_end: 0.0
                  muQ_step: 150.0
        components:
          - type: group
            name: run_cmf_test
            group:
              - cmf
    ''', '''
        processes:
          - name: lepton-module
            module: lepton
            config:
              global:
                use_beta_equilibrium: false
                use_charge_neutrality: true
            inputs:
              input_eos:
                type: job
                uuid: saved_job_id
                path: /cmf/opt/output/CMF_output_for_Lepton_baryons.csv
        components:
          - type: group
            name: lepton
            group:
              - lepton-module
    ''']
    return yaml.safe_load(wf_configs[idx])


class SaveJob(TestCase):
    def setUp(self):
        set_env_vars()
        purge_and_create_users()
        self.apis = get_staff_apis()

    def test_saved_job_limit(self):
        usernames = [username for username in self.apis]
        user_0 = usernames[0]
        api = self.apis[user_0]
        limits = get_usage_limits()
        max_saved_jobs = limits['saved_jobs']['limit']
        # Launch more jobs than the saved job limit
        wf_config = get_workflow_config(idx=0)
        responses = {}
        job_idx = 0
        while job_idx < max_saved_jobs + 1:
            response = api.job_create(config={
                "workflow": {
                    "config": wf_config
                }
            })
            self.assertTrue('uuid' in response)
            job_id = response['uuid']
            self.assertIsInstance(job_id, str)
            responses[job_idx] = response
            job_idx += 1
        # Attempt to set all jobs to public. The last attempt should be denied.
        for job_idx, info in responses.items():
            job_id = info['uuid']
            response = api.update_job(job_id=job_id, saved=True)
            if job_idx >= max_saved_jobs:
                self.assertEqual(response.status_code, 403)
            else:
                self.assertTrue('saved' in response)
                self.assertTrue(response['saved'] is True)
        # Unsave the first job so that the last job can be saved.
        job_id = responses[0]['uuid']
        response = api.update_job(job_id=job_id, saved=False)
        self.assertTrue('saved' in response)
        self.assertTrue(response['saved'] is False)
        job_id = responses[len(responses) - 1]['uuid']
        response = api.update_job(job_id=job_id, saved=True)
        self.assertTrue('saved' in response)
        self.assertTrue(response['saved'] is True)
        # An attempt to resave the first job should fail.
        job_id = responses[0]['uuid']
        response = api.update_job(job_id=job_id, saved=True)
        self.assertEqual(response.status_code, 403)

    def test_saved_job(self):
        usernames = [username for username in self.apis]
        user_0 = usernames[0]
        user_1 = usernames[1]

        # Launch a new job
        #
        # Acting as user_0
        api = self.apis[user_0]
        wf_config = get_workflow_config(idx=0)
        response = api.job_create(config={
            "workflow": {
                "config": wf_config
            }
        })
        self.assertTrue('uuid' in response)
        job_id = response['uuid']
        self.assertIsInstance(job_id, str)

        # Job owner should be able to fetch the job info
        #
        response = api.job_list(uuid=job_id)
        self.assertTrue('saved' in response)
        self.assertEqual(response['saved'], False)
        self.assertTrue('public' in response)
        self.assertEqual(response['public'], False)

        # Another user should not be able to change the job properties
        #
        # Acting as user_1
        api = self.apis[user_1]
        response = api.update_job(job_id=job_id, saved=True)
        self.assertEqual(response.status_code, 404)
        response = api.update_job(job_id=job_id, public=True)
        self.assertEqual(response.status_code, 404)

        # Another user should not be able to fetch the job info
        #
        response = api.job_list(uuid=job_id)
        self.assertEqual(response.status_code, 404)

        # Job owner changes the visibility of the job to public
        #
        # Acting as user_0
        api = self.apis[user_0]
        response = api.update_job(job_id=job_id, public=True)
        self.assertTrue('saved' in response)
        self.assertEqual(response['saved'], False)
        self.assertTrue('public' in response)
        self.assertEqual(response['public'], True)

        # Another user should still not be able to change the job properties
        #
        # Acting as user_1
        api = self.apis[user_1]
        response = api.update_job(job_id=job_id, saved=True)
        self.assertEqual(response.status_code, 404)
        response = api.update_job(job_id=job_id, public=True)
        self.assertEqual(response.status_code, 404)

        # Job owner should be able to save the job
        #
        # Acting as user_0
        api = self.apis[user_0]
        response = api.update_job(job_id=job_id, saved=True)
        self.assertTrue('saved' in response)
        self.assertEqual(response['public'], True)
        self.assertTrue('public' in response)
        self.assertEqual(response['saved'], True)

        job_id_0 = job_id

        # Poll the first job status until the job is complete
        #
        count = 0
        loops = 40
        sleep = 3
        while count < loops and response['status'] not in ['SUCCESS', 'FAILURE']:
            time.sleep(sleep)
            response = api.job_list(uuid=job_id_0)
            # print(response)
            count += 1
        self.assertTrue(response['status'] == 'SUCCESS')

        # Another user can launch a workflow that consumes the public job output
        #
        # Acting as user_1
        api = self.apis[user_1]

        # Another user should not be able to delete a public job
        response = api.job_delete(uuid=job_id_0)
        # print(response)
        self.assertEqual(response.status_code, 404)

        wf_config = get_workflow_config(idx=1)
        wf_config['processes'][0]['inputs']['input_eos']['uuid'] = job_id_0
        response = api.job_create(config={
            "workflow": {
                "config": wf_config
            }
        })
        # print(response)
        self.assertTrue('uuid' in response)
        job_id_1 = response['uuid']
        self.assertIsInstance(job_id_1, str)

        count = 0
        loops = 40
        sleep = 3
        while count < loops and response['status'] not in ['SUCCESS', 'FAILURE']:
            time.sleep(sleep)
            response = api.job_list(uuid=job_id_1)
            count += 1
        self.assertTrue(response['status'] == 'SUCCESS')

        # Job owner changes the visibility of the job to private
        #
        # Acting as user_0
        api = self.apis[user_0]
        response = api.update_job(job_id=job_id_0, public=False)
        self.assertTrue('saved' in response)
        self.assertEqual(response['saved'], True)
        self.assertTrue('public' in response)
        self.assertEqual(response['public'], False)

        # Another user should not be able to consume the private job output
        #
        # Acting as user_1
        api = self.apis[user_1]
        response = api.job_create(config={
            "workflow": {
                "config": wf_config
            }
        })
        # print(response)
        self.assertTrue('uuid' in response)
        job_id_1 = response['uuid']
        self.assertIsInstance(job_id_1, str)

        count = 0
        loops = 40
        sleep = 3
        while count < loops and response['status'] not in ['SUCCESS', 'FAILURE']:
            time.sleep(sleep)
            response = api.job_list(uuid=job_id_1)
            count += 1
        self.assertTrue(response['status'] == 'FAILURE')

        # The owner should be able to delete the original job
        #
        # Acting as user_0
        api = self.apis[user_0]
        response = api.job_delete(uuid=job_id_0)
        # print(response)
        self.assertEqual(response.status_code, 204)
