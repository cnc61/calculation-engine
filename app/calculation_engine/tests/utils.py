import os
import sys
from calculation_engine.tests.calculation_engine_api import CalculationEngineApi
import logging
logging.basicConfig(
    level=os.getenv('LOG_LEVEL', logging.DEBUG),
    stream=sys.stdout)
logger = logging.getLogger(__name__)


class Users():
    staff = {
        'foofoo': {
            'password': 'password',
            'first_name': 'Little',
            'last_name': 'Bunny',
        },
        'titus': {
            'password': 'password',
            'first_name': 'Titus',
            'last_name': 'Andronicus',
        },
    }


def purge_and_create_users():
    admin_api = CalculationEngineApi()
    for username, info in Users.staff.items():
        admin_api.delete_user(username)
        admin_api.create_user(
            username=username,
            password=info['password'],
            first_name=info['first_name'],
            last_name=info['last_name'],
        )


def get_admin_api(conf={}):
    return CalculationEngineApi(conf=conf)


def get_staff_apis(username: str = '') -> dict:
    staff_api = {}
    if username:
        if username in Users.staff:
            api = CalculationEngineApi({
                'username': username,
                'password': Users.staff[username]['password'],
            })
            return {username: api}
        else:
            return {}
    for username, info in Users.staff.items():
        staff_api[username] = CalculationEngineApi({
            'username': username,
            'password': info['password'],
            'last_name': info['last_name'],
            'first_name': info['first_name'],
        })
    return staff_api


def purge_all_user_data(apis) -> None:
    # Delete all jobs and uploaded files for the users
    for username, api in apis.items():
        logger.info(f'\nDelete all jobs "{username}":\n')
        api.job_delete_all()
        logger.info(f'\nList all jobs for "{username}":\n')
        api.job_list()
        logger.info(f'\nList current uploaded files for "{username}":\n')
        uploads = api.upload_list()
        logger.info(f'\nDelete all uploaded files for "{username}":\n')
        for upload in uploads:
            api.delete_uploaded_file(uuid=upload['uuid'])


def set_env_vars():
    api_server_port = os.environ.get('API_SERVER_PORT', '8000')
    api_server_host = os.environ.get('API_SERVER_HOST', 'localhost')
    api_url_authority = f'{api_server_host}:{api_server_port}'
    os.environ['CE_API_URL_AUTHORITY'] = api_url_authority
