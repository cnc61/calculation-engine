
from django.test import TestCase
from calculation_engine.tests.utils import purge_and_create_users, set_env_vars, get_staff_apis
import yaml
import time
import os
import uuid

import hashlib
import logging
import sys

logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)


class ErrorTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        set_env_vars()
        purge_and_create_users()
        cls.apis = get_staff_apis()

        # Upload dummy file
        cls.upload_response = cls.upload_dummy_file()

        # Run and save CMF workflow
        cls.cmf_response = cls.run_workflow(cls.cmf_wf())
        assert cls.cmf_response['status'] == 'SUCCESS', "CMF workflow setup failed."

        usernames = [username for username in cls.apis]
        user_0 = usernames[0]
        api = cls.apis[user_0]
        api.update_job(job_id=cls.cmf_response['uuid'], saved=True)

    @classmethod
    def upload_dummy_file(cls):
        usernames = [username for username in cls.apis]
        user_0 = usernames[0]
        api = cls.apis[user_0]

        dummy_data = '/tmp/dummy.dat'
        if not os.path.exists(dummy_data):
            with open(dummy_data, 'wb') as f:
                f.write(os.urandom(10 * 1024 * 1024))

        upload_response = api.upload_file(
            file_path=dummy_data,
            upload_path=dummy_data,
            description=dummy_data,
            public=False,
        )
        return upload_response

    @staticmethod
    def generate_checksum():
        random_data = os.urandom(128)
        return hashlib.md5(random_data).hexdigest()

    @staticmethod
    def generate_uuid():
        return str(uuid.uuid4())

    @classmethod
    def cmf_wf(cls):
        return yaml.safe_load('''
            processes:
              - name: cmf
                module: cmf_solver
                config:
                  variables:
                    chemical_optical_potentials:
                      muB_begin: 1000.0
                      muB_end: 1400.0
                      muB_step: 200.0
                      use_hyperons: false
                      use_decuplet: false
                      use_quarks: false
            components:
              - type: group
                name: run_cmf_test
                group:
                  - cmf
        ''')

    def lepton_wf(self, id, checksum=None, path=None):
        if path and not checksum:
            inputs_config = f'''
                input_eos:
                  type: 'job'
                  uuid: {id}
                  path: {path}
            '''
        elif checksum and not path:
            inputs_config = f'''
                input_eos:
                  type: 'upload'
                  uuid: {id}
                  checksum: {checksum}
            '''
        else:
            return None
        return yaml.safe_load(f'''
            processes:
            - name: lepton-run
              module: lepton
              config:
                global:
                  use_charge_neutrality: false
              inputs:
                {inputs_config}
            components:
            - type: group
              name: run_lepton
              group:
              - lepton-run
        ''')

    def synthesis_wf(self, id, checksum, synthesis_type='maxwell'):
        return yaml.safe_load(f'''
            processes:
            - name: synthesis-run
              module: synthesis
              config:
                global:
                  synthesis_type: {synthesis_type}
              inputs:
                model1_BetaEq_eos:
                  type: upload
                  uuid: {id}
                  checksum: {checksum}
                model2_BetaEq_eos:
                  type: upload
                  uuid: {id}
                  checksum: {checksum}
            components:
            - type: group
              name: run_synthesis
              group:
              - synthesis-run
        ''')

    @classmethod
    def run_workflow(cls, wf_config, timeout=300):
        usernames = [username for username in cls.apis]
        user_0 = usernames[0]
        api = cls.apis[user_0]

        response = api.job_create(config={
            "workflow": {
                "config": wf_config
            }
        })

        if 'uuid' not in response:
            return response

        job_id = response['uuid']

        time_sleep = 5  # seconds
        time_start = time.time()
        response = api.job_list(uuid=job_id)
        while timeout > time.time() - time_start and response['status'] not in ['SUCCESS', 'FAILURE']:
            time.sleep(time_sleep)
            response = api.job_list(uuid=job_id)
        return response

    def test_invalid_checksum(self):
        response = self.run_workflow(self.lepton_wf(
            id=self.upload_response['uuid'], checksum=self.generate_checksum()))
        self.assertEqual(response['status'], 'FAILURE')
        self.assertIn('Checksum mismatch', response['error_info'])

    def test_invalid_upload_id(self):
        response = self.run_workflow(self.lepton_wf(
            id=self.generate_uuid(), checksum=self.upload_response['checksum']))
        self.assertEqual(response['status'], 'FAILURE')
        self.assertIn('upload uuid not found', response['error_info'])

    def test_invalid_saved_uuid(self):
        self.assertEqual(self.cmf_response['status'], 'SUCCESS')
        path = '/opt/output/CMF_output_for_Lepton_baryons.csv'
        response = self.run_workflow(self.lepton_wf(
            id=self.generate_uuid(), path=path))
        self.assertTrue(response['status'], 'FAILURE')
        self.assertIn('saved job not found or is private', response['error_info'])

    def test_invalid_saved_path(self):
        self.assertEqual(self.cmf_response['status'], 'SUCCESS')
        wrong_path = '/opt/output/wrong-path'
        response = self.run_workflow(self.lepton_wf(
            id=self.cmf_response['uuid'], path=wrong_path))
        self.assertEqual(response['status'], 'FAILURE')
        self.assertIn('saved job file not found', response['error_info'])

    def test_missing_required_input(self):
        wf_config = self.synthesis_wf(id=self.upload_response['uuid'], checksum=self.upload_response['checksum'])
        del wf_config['processes'][0]['inputs']['model2_BetaEq_eos']
        response = self.run_workflow(wf_config)
        self.assertEqual(response['status'], 'FAILURE')
        self.assertIn('Required input is missing', response['error_info'])

    def test_status_error(self):
        wf_config = self.synthesis_wf(id=self.upload_response['uuid'], checksum=self.upload_response['checksum'],
                                      synthesis_type='throw-error')
        response = self.run_workflow(wf_config)
        self.assertEqual(response['status'], 'FAILURE')
        self.assertIn('Internal failure', response['error_info'])

    # TODO: Add test for invalid component
    # I am not sure how to test this, as no response is returned when the component is invalid.
    # Error: UnboundLocalError: cannot access local variable 'response' where it is not associated with a value
    # def test_invalid_component(self):
    #     wf_config=self.lepton_wf(id=self.upload_response['uuid'], checksum=self.upload_response['checksum'])
    #     wf_config['components'][0]['group'][0] = 'lepton-no-run'

    #     logging.debug(response)
    #     response = self.run_workflow(wf_config)
    #     # self.assertEqual(response['status'], 'FAILURE')

    #     self.assertIn('Invalid workflow config', response['error_info'])
