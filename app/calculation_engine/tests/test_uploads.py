from django.test import TestCase
from calculation_engine.tests.utils import purge_and_create_users, get_admin_api, get_staff_apis, set_env_vars
import os
from uuid import uuid4
from time import sleep


def upload_test_file(api, path='', file_path='', size=100):
    if not path:
        path = '/test/upload.txt'
    # Delete upload at given path if it already exists
    uploads = api.upload_list()
    for upload in uploads:
        if upload['path'] == path:
            api.delete_uploaded_file(uuid=upload['uuid'])
    if not file_path:
        uuid = str(uuid4()).replace("-", "")
        file_path = f'/tmp/upload-test.{size}.{uuid}.dat'
    with open(file_path, 'wb') as fh:
        fh.write(os.urandom(size))
    assert os.path.exists(file_path)
    # print(os.stat(file_path))
    response = api.upload_file(
        file_path=file_path,
        upload_path=path,
        description='Test of the upload system',
        public=False,
    )
    # Delete local file
    os.unlink(file_path)
    return response


class UploadCreate(TestCase):
    def setUp(self):
        set_env_vars()
        self.admin_api = get_admin_api()

    def test_upload_create(self):
        response = upload_test_file(api=self.admin_api)
        self.assertTrue('uuid' in response)
        upload_id = response['uuid']
        self.assertIsInstance(upload_id, str)


class UploadList(TestCase):
    def setUp(self):
        set_env_vars()
        # Create test user accounts
        purge_and_create_users()
        self.apis = get_staff_apis()

    def test_upload_list(self):
        usernames = [username for username in self.apis]
        user_0 = usernames[0]
        user_1 = usernames[1]

        # Acting as user_0
        api = self.apis[user_0]
        response = upload_test_file(api=api)

        self.assertTrue('uuid' in response)
        upload_id = response['uuid']
        self.assertIsInstance(upload_id, str)

        # Upload owner should fetch a finite set of uploads
        uploads = api.upload_list()
        self.assertTrue(uploads)

        # Another user should fetch an empty set of uploads
        # Acting as user_1
        api = self.apis[user_1]
        uploads = api.upload_list()
        self.assertFalse(uploads)

        # Upload owner should retrieve the upload info by UUID
        # Acting as user_0
        api = self.apis[user_0]
        response = api.upload_list(uuid=upload_id)
        self.assertTrue('uuid' in response)

        # Another user should not retrieve the upload info by UUID
        # Acting as user_1
        api = self.apis[user_1]
        response = api.upload_list(uuid=upload_id)
        self.assertEqual(response.status_code, 404)


class UploadDelete(TestCase):
    def setUp(self):
        set_env_vars()
        # Create test user accounts
        purge_and_create_users()
        self.apis = get_staff_apis()

    def test_upload_delete(self):
        usernames = [username for username in self.apis]
        user_0 = usernames[0]
        user_1 = usernames[1]

        # Acting as user_0
        # Upload a file and then delete it
        api = self.apis[user_0]
        response = upload_test_file(api=api)

        self.assertTrue('uuid' in response)
        upload_id = response['uuid']
        self.assertIsInstance(upload_id, str)

        # Owner deletes the upload
        response = api.delete_uploaded_file(uuid=upload_id)
        self.assertEqual(response.status_code, 200)

        # Owner fetches empty list of uploads
        uploads = api.upload_list()
        self.assertFalse(uploads)

        # Upload a new file and then let other user try to delete it
        response = upload_test_file(api=api)
        self.assertTrue('uuid' in response)
        upload_id = response['uuid']
        self.assertIsInstance(upload_id, str)

        # Another user should fail to delete the upload
        # Acting as user_1
        api = self.apis[user_1]
        response = api.delete_uploaded_file(uuid=upload_id)
        self.assertEqual(response.status_code, 404)


class UploadUpdate(TestCase):

    def setUp(self):
        set_env_vars()
        # Create test user accounts
        purge_and_create_users()
        self.apis = get_staff_apis()

    def test_upload_update(self):
        usernames = [username for username in self.apis]
        user_0 = usernames[0]
        user_1 = usernames[1]

        # Acting as user_0
        # Upload a file and then delete it
        api = self.apis[user_0]
        response = upload_test_file(api=api)

        self.assertTrue('uuid' in response)
        upload_id = response['uuid']
        self.assertIsInstance(upload_id, str)

        # Acting as user_0
        # Set the file to public
        api = self.apis[user_0]
        response = api.upload_file_update(
            uuid=upload_id,
            public=True,
            parse_json=False)
        self.assertEqual(response.status_code, 200)
        response = api.upload_list(uuid=upload_id)
        self.assertTrue('path' in response)

        response = api.upload_file_update(
            uuid=upload_id,
            description='New description',
            parse_json=False)
        self.assertEqual(response.status_code, 200)

        # Acting as user_1
        api = self.apis[user_1]
        response = api.upload_list(uuid=upload_id)
        self.assertTrue('description' in response)
        self.assertTrue(response['description'] == 'New description')


class UploadShare(TestCase):

    def setUp(self):
        set_env_vars()
        # Create test user accounts
        purge_and_create_users()
        self.apis = get_staff_apis()

    def test_upload_share(self):
        usernames = [username for username in self.apis]
        user_0 = usernames[0]
        user_1 = usernames[1]

        # Acting as user_0
        # Upload a file and then delete it
        api = self.apis[user_0]
        response = upload_test_file(api=api)

        self.assertTrue('uuid' in response)
        upload_id = response['uuid']
        self.assertIsInstance(upload_id, str)

        # Another user should see no uploads
        # Acting as user_1
        api = self.apis[user_1]
        uploads = api.upload_list()
        self.assertFalse(uploads)
        response = api.upload_list(uuid=upload_id)
        # print(f'''\n{uploads}\n''')
        self.assertEqual(response.status_code, 404)

        # Acting as user_0
        # Set the file to public
        api = self.apis[user_0]
        response = api.upload_file_update(
            uuid=upload_id,
            public=True,
            parse_json=False)
        self.assertEqual(response.status_code, 200)
        response = api.upload_list(uuid=upload_id)
        # print(f'''\n{response}\n''')
        self.assertTrue('path' in response)

        # Another user should see the upload
        # Acting as user_1
        api = self.apis[user_1]
        response = api.upload_list(uuid=upload_id)
        # print(f'''\n{response}\n''')
        self.assertTrue('path' in response)

        # Another user should fail to delete the upload
        response = api.delete_uploaded_file(uuid=upload_id)
        self.assertEqual(response.status_code, 404)

        # Acting as user_0
        api = self.apis[user_0]
        # Owner deletes the upload
        response = api.delete_uploaded_file(uuid=upload_id)
        self.assertEqual(response.status_code, 200)
        # Owner fetches empty list of uploads
        uploads = api.upload_list()
        self.assertFalse(uploads)


class UploadSizeLimit(TestCase):

    def setUp(self):
        set_env_vars()
        # Create test user accounts
        purge_and_create_users()
        self.apis = get_staff_apis()

    def test_upload_size_limit(self):
        usernames = [username for username in self.apis]
        user_0 = usernames[0]
        # Acting as user_0
        api = self.apis[user_0]
        # Delete any existing uploads
        uploads = api.upload_list()
        for upload in uploads:
            api.delete_uploaded_file(uuid=upload['uuid'])
        # Upload a file one byte less than the limit, then another to put it one byte over the limit.
        # This should be allowed because the upload size check does not include the current upload.
        limit_in_GiB = 1
        for size_in_MiB in [(limit_in_GiB * 1024) - 1, 2]:
            response = upload_test_file(api=api,
                                        path=f'/test-upload-{str(uuid4()).replace("-", "")}',
                                        size=size_in_MiB * 1024**2)
            self.assertTrue('uuid' in response)
            upload_id = response['uuid']
            self.assertIsInstance(upload_id, str)
        # Subsequent uploads should fail because the limit has been exceeded.
        sleep(1)
        size_in_MiB = 1
        response = upload_test_file(api=api,
                                    path=f'/test-upload-{str(uuid4()).replace("-", "")}',
                                    size=size_in_MiB * 1024**2)
        self.assertEqual(response.status_code, 403)
        # Delete the 2-MiB upload
        api.delete_uploaded_file(uuid=upload_id)
        # Now the 1-MiB upload will succeed because the user is under threshold again
        size_in_MiB = 1
        sleep(1)
        response = upload_test_file(
            api=api,
            path=f'/test-upload-{str(uuid4()).replace("-", "")}',
            size=size_in_MiB * 1024**2)
        self.assertTrue('uuid' in response)
        upload_id = response['uuid']
        self.assertIsInstance(upload_id, str)
