from django.test import TestCase
from calculation_engine.tests.utils import purge_and_create_users, set_env_vars, get_staff_apis
import yaml
import time
import os
from pathlib import Path
import shutil
import urllib
import random
from rest_framework import status
from app_base import settings


def load_config_file(config_path=settings.CE_CONFIG_PATH):
    # NOTE: This does not properly load the full config because
    #       it ignores the override values that may be in a
    #       secret config.
    with open(config_path, "r") as conf_file:
        config = yaml.load(conf_file, Loader=yaml.FullLoader)
    return config


class WorkflowTests(TestCase):
    def setUp(self):
        set_env_vars()
        purge_and_create_users()
        self.apis = get_staff_apis()

    def run_workflow(self, workflow_config_file, check_files=[], timeout=300):
        usernames = [username for username in self.apis]
        user_0 = usernames[0]
        api = self.apis[user_0]
        with open(workflow_config_file) as fh:
            wf_config = yaml.safe_load(fh)
        response = api.job_create(config={
            "workflow": {
                "config": wf_config
            }
        })
        self.assertTrue('uuid' in response)
        job_id = response['uuid']
        self.assertIsInstance(job_id, str)

        time_sleep = 5  # seconds
        time_start = time.time()
        response = api.job_list(uuid=job_id)
        while timeout > time.time() - time_start and response['status'] not in ['SUCCESS', 'FAILURE']:
            time.sleep(time_sleep)
            response = api.job_list(uuid=job_id)
        self.assertTrue(response['status'] == 'SUCCESS')

        for output_path in check_files:
            # Download check file
            api.download_job_file(job_id, output_path, root_dir='/tmp')
            file_path = os.path.join('/tmp', job_id, output_path.strip('/'))
            self.assertTrue(os.path.isfile(file_path))
            # Delete downloaded files
            shutil.rmtree(os.path.join('/tmp', job_id))

    def upload_input_file(self, url, upload_path, file_name=None):
        # Upload the file to the object store and get the upload id
        usernames = [username for username in self.apis]
        user_0 = usernames[0]
        api = self.apis[user_0]
        search = [upload['uuid'] for upload in api.upload_list() if upload['path'] == upload_path]
        if search:
            print(f'''File already uploaded: {upload_path}''')
            return search[0]
        # Download EoS table
        if not file_name:
            file_name = 'input_test_file'
        file_path = os.path.join('/tmp', file_name)
        if os.path.exists(file_path):
            print(f'''Input file already downloaded: "{file_name}"''')
        else:
            print(f'''Downloading input file from "{url}"...''')
            urllib.request.urlretrieve(url, file_path)
        print(f'''Uploading input to "{upload_path}"...''')
        upload_response = api.upload_file(
            file_path=file_path,
            public=True,
            upload_path=upload_path,
            description='Workflow test input file',
        )
        return upload_response['uuid']

    def crust_dft_eos_input(self, wf_config):
        if (wf_config['processes'][0]['config']['generate_table'] is False
                or wf_config['processes'][0]['config']['ext_guess'] is True):  # noqa W503
            for process in wf_config['processes']:
                if process['name'] == 'crust_dft_eos':
                    process['inputs'] = {
                        'EOS_table': {
                            'type': 'upload',
                            'checksum': '164575f9d84c3ac087780e0219ee2e8a',
                            'uuid': self.upload_input_file(
                                url='https://isospin.roam.utk.edu/public_data/eos_tables/du21/fid_3_5_22.o2',
                                upload_path='/crust_dft/data/fid_3_5_22.o2',
                                file_name='fid_3_5_22.o2')
                        }
                    }
        return wf_config

    def test_lepton(self):
        workflow_config_file = os.path.join(
            Path(__file__).resolve().parent,
            'workflows/singletons/lepton/workflow_config.yaml')
        self.run_workflow(workflow_config_file, check_files=[
            '/lepton-module/opt/output/lepton_eos.csv',
        ])

    def test_cmf(self):
        workflow_config_file = os.path.join(
            Path(__file__).resolve().parent,
            'workflows/singletons/cmf/workflow_config.yaml')
        self.run_workflow(workflow_config_file, check_files=[
            '/cmf/opt/output/CMF_output_stable.csv',
        ])

    def test_holographic_eos(self):
        workflow_config_file = os.path.join(
            Path(__file__).resolve().parent,
            'workflows/singletons/holographic_eos/workflow_config.yaml')
        self.run_workflow(workflow_config_file, check_files=[
            '/holographic/home/eos/bh_eos/output/transition_line.csv',
        ])

    def test_eos_ising_texs_2d(self):
        workflow_config_file = os.path.join(
            Path(__file__).resolve().parent,
            'workflows/singletons/eos_ising_texs_2d/workflow_config.yaml')
        self.run_workflow(workflow_config_file, check_files=[
            '/eos_ising_texs_2d/opt/output/Ising-TExS_output_thermodynamics.csv',
        ])

    def test_eos_taylor_4d(self):
        workflow_config_file = os.path.join(
            Path(__file__).resolve().parent,
            'workflows/singletons/eos_taylor_4d/workflow_config.yaml')
        self.run_workflow(workflow_config_file, check_files=[
            '/eos_taylor_4d/opt/output/BQS_eos_output_thermodynamics.csv',
        ])

    def test_chiral_eft(self):
        workflow_config_file = os.path.join(
            Path(__file__).resolve().parent,
            'workflows/singletons/chiral_eft/workflow_config.yaml')
        self.run_workflow(workflow_config_file, check_files=[
            '/chiral_eft_eos/opt/output/output_stable.csv',
            '/chiral_eft_eos/opt/output/output_lepton.csv',
            '/chiral_eft_eos/opt/output/output_flavor_equilibration.csv',
        ])

    def test_cmf_lepton_synthesis_qlimr(self):
        workflow_config_file = os.path.join(
            Path(__file__).resolve().parent,
            'workflows/complex/cmf_lepton_synthesis_qlimr/workflow_config.yaml')
        self.run_workflow(workflow_config_file, check_files=[
            '/cmf/opt/output/CMF_output_for_Lepton_baryons.csv',
            '/lepton-module-baryons/opt/output/beta_equilibrium_eos.csv',
            '/lepton-module-quarks/opt/output/beta_equilibrium_eos.csv',
            '/synthesis-module/opt/output/eos.csv',
            '/qlimr/opt/output/observables.csv',
        ])

    def test_flavor_equilibration(self):
        workflow_config_file = os.path.join(
            Path(__file__).resolve().parent,
            'workflows/singletons/flavor_equilibration/workflow_config.yaml')
        with open(workflow_config_file) as fp:
            wf_config = yaml.load(fp, Loader=yaml.SafeLoader)
        wf_config['processes'][0]['inputs']['EoS']['uuid'] = self.upload_input_file(
            url='https://gitlab.com/nsf-muses/flavor-equilibration/beta_equil/'
            '-/raw/dc5910beef7512686ae5561fcbcd0b8ab60d73a1/input/IUF_Porter.csv?inline=false',
            upload_path='/flavor_equil/test/IUF_Porter.csv',
            file_name='IUF_Porter.csv')
        wf_config['processes'][0]['inputs']['EoS']['checksum'] = '88eee2c8dbdad22d9d78eba8c4749171'
        workflow_config_file = f'/tmp/flavor_equilibration.{random.randrange(10000, 99999)}.yaml'
        with open(workflow_config_file, 'w') as fp:
            yaml.dump(wf_config, fp, sort_keys=False)
        self.run_workflow(workflow_config_file, check_files=[
            '/flavor_equil/opt/output/flavor_equilibration_results',
            '/flavor_equil/opt/output/flavor_equilibration_results_feqformat',
            '/flavor_equil/opt/output/EoS_table_feqformat',
        ])

    def test_crust_dft(self):
        workflow_config_file = os.path.join(
            Path(__file__).resolve().parent,
            'workflows/singletons/crust_dft/workflow_config.yaml')
        with open(workflow_config_file) as fp:
            wf_config = yaml.load(fp, Loader=yaml.SafeLoader)
        wf_config = self.crust_dft_eos_input(wf_config)
        workflow_config_file = f'/tmp/crust_dft.{random.randrange(10000, 99999)}.yaml'
        with open(workflow_config_file, 'w') as fp:
            yaml.dump(wf_config, fp, sort_keys=False)
        self.run_workflow(workflow_config_file, check_files=[
            '/crust_dft_eos/opt/e4mma/output/crust_dft.csv',
        ])

    def test_cmf_lepton(self):
        workflow_config_file = os.path.join(
            Path(__file__).resolve().parent,
            'workflows/complex/cmf_lepton/workflow_config.yaml')
        self.run_workflow(workflow_config_file, check_files=[
            '/cmf/opt/output/CMF_output_for_Lepton_baryons.csv',
            '/lepton-module/opt/output/charge_neutrality_eos.csv',
        ])

    def test_chiral_eft_lepton(self):
        workflow_config_file = os.path.join(
            Path(__file__).resolve().parent,
            'workflows/complex/chiral_eft_lepton/workflow_config.yaml')
        self.run_workflow(workflow_config_file, check_files=[
            '/chiral_eft_eos/opt/output/output_lepton.csv',
            '/lepton-module/opt/output/beta_equilibrium_eos.csv',
        ])

    def test_crust_dft_lepton(self):
        workflow_config_file = os.path.join(
            Path(__file__).resolve().parent,
            'workflows/complex/crust_dft_lepton/workflow_config.yaml')
        with open(workflow_config_file) as fp:
            wf_config = yaml.load(fp, Loader=yaml.SafeLoader)
        wf_config = self.crust_dft_eos_input(wf_config)
        workflow_config_file = f'/tmp/crust_dft_lepton.{random.randrange(10000, 99999)}.yaml'
        with open(workflow_config_file, 'w') as fp:
            yaml.dump(wf_config, fp, sort_keys=False)
        self.run_workflow(workflow_config_file, check_files=[
            '/crust_dft_eos/opt/e4mma/output/crust_dft.csv',
            '/lepton-module/opt/output/beta_equilibrium_eos.csv',
        ])

    def test_crust_dft_cmf_lepton_synthesis_qlimr(self):
        workflow_config_file = os.path.join(
            Path(__file__).resolve().parent,
            'workflows/complex/crust_dft_cmf_lepton_synthesis_qlimr/workflow_config.yaml')
        with open(workflow_config_file) as fp:
            wf_config = yaml.load(fp, Loader=yaml.SafeLoader)
        wf_config = self.crust_dft_eos_input(wf_config)
        workflow_config_file = f'/tmp/crust_dft_cmf_lepton_synthesis_qlimr.{random.randrange(10000, 99999)}.yaml'
        with open(workflow_config_file, 'w') as fp:
            yaml.dump(wf_config, fp, sort_keys=False)
        self.run_workflow(workflow_config_file, check_files=[
            '/crust_dft_eos/opt/e4mma/output/crust_dft.csv',
            '/cmf/opt/output/CMF_output_for_Lepton_baryons.csv',
            '/lepton-module-cmf/opt/output/beta_equilibrium_eos.csv',
            '/lepton-module-crust_dft/opt/output/beta_equilibrium_eos.csv',
            '/synthesis-module/opt/output/eos.csv',
            '/qlimr/opt/output/observables.csv',
        ])

    def test_workflow_process_limit(self):
        # Obtain usage limit value
        # NOTE: This does not use the limits.get_usage_limits() function because
        #       that requires the context of a running Django instance; it is 
        #       convenient to run test/job_cannon.py independently which invokes
        #       these unit tests.
        config = load_config_file()
        max_process_instances = config['limits']['process_instances']['limits'][0]
        workflow_config_file = os.path.join(
            Path(__file__).resolve().parent,
            'workflows/complex/cmf_process_limit/workflow_config.yaml')
        with open(workflow_config_file) as fp:
            wf_config = yaml.load(fp, Loader=yaml.SafeLoader)

        # Run workflow with more than max process instances
        wf_config['components'][0]['group'] = ['cmf' for _ in range(0, max_process_instances + 1)]
        usernames = [username for username in self.apis]
        user_0 = usernames[0]
        api = self.apis[user_0]
        response = api.job_create(config={
            "workflow": {
                "config": wf_config
            }
        })
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Run workflow with max process instances
        wf_config['components'][0]['group'] = ['cmf' for _ in range(0, max_process_instances)]
        workflow_config_file = f'/tmp/test_workflow_process_limit.{random.randrange(10000, 99999)}.yaml'
        with open(workflow_config_file, 'w') as fp:
            yaml.dump(wf_config, fp, sort_keys=False)
        self.run_workflow(workflow_config_file, check_files=[
            '/cmf/opt/output/CMF_output_stable.csv',
        ])
