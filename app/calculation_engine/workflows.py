import yaml
from typing import Dict, List, TypedDict
from celery.canvas import Signature
from calculation_engine.tasks import run_module, workflow_complete, workflow_init, wf_error_handler
from celery import chain, group
import json
from calculation_engine.models import Job
from calculation_engine.models import update_job_state
from celery import shared_task
from calculation_engine.log import get_logger
from calculation_engine.limits import get_usage_limits
logger = get_logger(__name__)


@shared_task(name='Launch Workflow')
def launch_workflow(request_config={}, job_id=''):
    update_job_state(job_id, Job.JobStatus.PENDING)
    wf_config = request_config['workflow']['config']
    logger.debug(f'''Workflow config:\n{yaml.dump(wf_config, indent=2)}''')
    wf = Workflow(config=wf_config, job_id=job_id)
    logger.debug('''Running workflow...''')
    wf.run_workflow()


class Process:
    def __init__(self, conf: dict = {}) -> None:
        self.name: str = conf['name'] if 'name' in conf else ''
        self.module: str = conf['module'] if 'module' in conf else ''
        self.config: dict = conf['config'] if 'config' in conf else {}
        self.pipes: Dict[str, dict] = conf['pipes'] if 'pipes' in conf else {}
        self.inputs: Dict[str, dict] = conf['inputs'] if 'inputs' in conf else {}
        self.task: Signature = None


class Component:
    def __init__(self, conf: dict = {}) -> None:
        self.name: str = conf['name'] if 'name' in conf else ''
        self.type: str = conf['type'] if 'type' in conf else ''
        self.task: Signature = None


class GroupComponent(Component):
    def __init__(self, conf: dict = {}) -> None:
        super().__init__(conf)
        self.group: List[Process] = []


class ChainComponent(Component):
    def __init__(self, conf: dict = {}) -> None:
        super().__init__(conf)
        self.sequence: List[Component] = []


class Workflow:
    def __init__(self, config: dict = {}, job_id: str = '') -> None:
        self.job_id = job_id
        self.config = config
        self.processes: List[Process] = []
        self.num_process_instances: int = 0
        self.components: List[Component] = []
        err_msg = self.process_config(config)
        if err_msg:
            err_msg = f'Invalid workflow config: {err_msg}'
            logger.error(err_msg)
            raise Exception(err_msg)

    def __str__(self) -> str:
        return yaml.dump({
            'processes': self.processes,
            'components': self.components,
        })

    def find_by_name(self, name):
        process_search = [process for process in self.processes if process.name == name]
        component_search = [component for component in self.components if component.name == name]
        if process_search or component_search:
            # There should only be a single process or component with the name being searched
            return process_search[0] if process_search else component_search[0]
        else:
            return None

    def process_config(self, config: Dict[str, list]) -> None:
        class ProcessConfig(TypedDict):
            name: str
            module: str
            pipes: dict
            inputs: dict
            config: dict

        class ComponentConfig(TypedDict):
            name: str
            type: str

        # Import process definitions
        confs: List[ProcessConfig] = config['processes']
        for conf in confs:
            # Process names must be unique
            name = conf['name']
            assert not self.find_by_name(name)
            # Load default values
            process = Process(conf)
            process.task = run_module.si(
                job_id=self.job_id,
                process_name=process.name,
                module_name=process.module,
                config=process.config,
                pipes=process.pipes,
                inputs=process.inputs,
            )
            self.processes.append(process)
            # logger.debug(self.processes)

        # Import workflow component definitions
        confs: List[ComponentConfig] = config['components']
        for conf in confs:
            name = conf['name']
            # Component must have a unique name
            try:
                assert not self.find_by_name(name)
            except AssertionError:
                err_msg = f'Component name must be unique: "{name}".'
                logger.error(err_msg)
                return err_msg
            component_type = conf['type']
            try:
                assert component_type in ['group', 'chain']
            except AssertionError:
                err_msg = f'Invalid component type: "{component_type}".'
                logger.error(err_msg)
                return err_msg
            if component_type == 'group':
                component = GroupComponent(conf)
                # logger.debug(str(component))
                for item_name in conf['group']:
                    try:
                        item = self.find_by_name(item_name)
                        assert item
                    except AssertionError:
                        err_msg = f'''Component or process not defined: "{item_name}".'''
                        logger.error(err_msg)
                        return err_msg
                    component.group.append(item)
                    # logger.debug(f'component.group: {component.group}')
                    # Count the number of process instances to enforce usage limits
                    if isinstance(item, Process):
                        self.num_process_instances += 1
                component.task = group([item.task for item in component.group])
            elif component_type == 'chain':
                component = ChainComponent(conf)
                for item_name in conf['sequence']:
                    # Sequence items may be names of components or processes
                    try:
                        item = self.find_by_name(item_name)
                        assert item
                    except AssertionError:
                        err_msg = f'''Component or process not defined: "{item_name}".'''
                        logger.error(err_msg)
                        return err_msg
                    component.sequence.append(item)
                    # Count the number of process instances to enforce usage limits
                    if isinstance(item, Process):
                        self.num_process_instances += 1
                component.task = chain([item.task for item in component.sequence])
            self.components.append(component)
            # logger.debug(self.components)
        return None

    def run_workflow(self) -> None:
        def find_task_ids(tasks_obj, task_ids=None):
            if task_ids is None:
                task_ids = []
            if isinstance(tasks_obj, list):
                for tasks_obj_item in tasks_obj:
                    task_ids = find_task_ids(tasks_obj_item, task_ids.copy())
            elif isinstance(tasks_obj, dict):
                for tasks_obj_key, tasks_obj_val in tasks_obj.items():
                    if tasks_obj_key == 'task_id' and isinstance(tasks_obj_val, str) and tasks_obj_val not in task_ids:
                        task_ids.append(tasks_obj_val)
                    else:
                        task_ids = find_task_ids(tasks_obj_val, task_ids.copy())
            return task_ids

        task = self.components[-1].task
        workflow = chain(
            workflow_init.si(job_id=self.job_id, config=self.config),
            task,
            workflow_complete.si(job_id=self.job_id),
        )
        # Use the freeze() function to provision static task IDs so they can
        # be revoked if the job is deleted before the workflow completes.
        workflow.freeze()
        workflow.on_error(wf_error_handler.s(job_id=self.job_id)).apply_async()
        # Collect all workflow task IDs and store with job so
        # they can be revoked when a job is deleted.
        workflow_task_ids = find_task_ids(workflow.tasks)
        logger.debug('Workflow task_ids:')
        logger.debug(json.dumps(workflow_task_ids, indent=2))
        job = Job.objects.get(uuid__exact=self.job_id)
        job.task_ids = workflow_task_ids
        job.save()
