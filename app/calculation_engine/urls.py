from calculation_engine import views
from rest_framework import routers
from django.urls import path, include
from django.contrib.auth.decorators import login_required

job_api_router = routers.DefaultRouter()
job_api_router.register('', views.JobViewSet, basename='job')

upload_list = views.UploadViewSet.as_view({
    'get': 'list',
    'put': 'create'
})
upload_detail = views.UploadViewSet.as_view({
    'get': 'retrieve',
    'patch': 'partial_update',
    'delete': 'destroy'
})
upload_api_urlpatterns = [
    path('', upload_list, name='upload-list'),
    path('<str:pk>/', upload_detail, name='upload-detail'),
]
metrics_list = views.MetricViewSet.as_view({
    'get': 'list',
})
metrics_api_urlpatterns = [
    path('', metrics_list, name='metrics-list'),
]
api_urlpatterns = [
    path('job/', include(job_api_router.urls)),
    path('upload/', include(upload_api_urlpatterns)),
    path('metrics/', include(metrics_api_urlpatterns)),
    path('module/', views.ModuleListView, name='module-list-json'),
]

jobfile_detail = views.JobFileDownloadViewSet.as_view({
    'get': 'download',
    'post': 'download',
})

page_urlpatterns = [
    path('modules/', views.ModuleListView, name='module-list'),
    path('limits/', views.UsageLimitListView, name='limits-list'),
    path('jobs/', login_required(views.JobListView.as_view()), name='job-page'),
    path('uploads/', login_required(views.UploadListView.as_view()), name='upload-page'),
    path('metrics/', login_required(views.MetricListView.as_view()), name='metrics-page'),
    path('download/<uuid:job_id>/<path:file_path>', jobfile_detail, name='download-job-file'),
    path('download/<uuid:upload_id>', views.UploadDownloadViewSet.as_view({
        'get': 'download',
        'post': 'download',
    }), name='download-upload'),
]
