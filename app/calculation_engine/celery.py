import os
from celery import Celery

# Set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'app_base.settings')

rabbitmq_username = os.environ.get('RABBITMQ_DEFAULT_USER', 'rabbitmq')
rabbitmq_password = os.environ.get('RABBITMQ_DEFAULT_PASS', 'rabbitmq')
rabbitmq_service = os.environ.get('MESSAGE_BROKER_HOST', 'rabbitmq')
rabbitmq_port = os.environ.get('MESSAGE_BROKER_PORT', '5672')

app = Celery('calculation_engine',
             broker=f'amqp://{rabbitmq_username}:{rabbitmq_password}@{rabbitmq_service}:{rabbitmq_port}//',
             include=[
                 'calculation_engine.workflows',
                 'calculation_engine.tasks',
                 'calculation_engine.tasks_api'
             ])

# ref: https://docs.celeryq.dev/en/stable/django/first-steps-with-django.html#using-celery-with-django
#
# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django apps.
app.autodiscover_tasks()

# Optional configuration, see the application user guide.
app.conf.update(
    result_expires=3600,
    task_default_queue='jobs',
    task_track_started=True,
    # TODO: Set soft time limit and ensure it works. See https://docs.celeryq.dev/en/stable/userguide/configuration.html#std-setting-task_soft_time_limit
    # task_soft_time_limit=5,
)

# If the worker is running in Kubernetes, enable the liveness probe
if os.getenv('KUBERNETES_SERVICE_HOST', ''):
    from app_base.k8s import LivenessProbe
    app.steps["worker"].add(LivenessProbe)


if __name__ == '__main__':
    app.start()
