from celery import shared_task
from calculation_engine.models import Job, Metric, Upload, JobFile
from django.contrib.auth.models import User
from app_base import settings
from datetime import datetime, timedelta, timezone
from calculation_engine.tasks_api import delete_job_files
from calculation_engine.tasks import delete_job_scratch_files
import os
import re
from calculation_engine.object_store import ObjectStore
from calculation_engine.log import get_logger
from pathlib import Path
logger = get_logger(__name__)


class PruneOrphanedData():

    @property
    def task_name(self):
        return "Prune orphaned data"

    @property
    def task_handle(self):
        return self.task_func

    @property
    def task_frequency_seconds(self):
        return settings.CE_JOB_PURGE_INTERVAL

    @property
    def task_initially_enabled(self):
        return True

    def __init__(self, task_func='') -> None:
        self.task_func = task_func

    def run_task(self):
        logger.info(f'Running periodic task "{self.task_name}"...')

        # Collect all finished jobs
        all_jobs = Job.objects.all()
        all_job_ids = [str(job.uuid) for job in all_jobs]
        finished_jobs = [job for job in all_jobs if job.status in [Job.JobStatus.SUCCESS,
                                                                   Job.JobStatus.FAILURE,
                                                                   Job.JobStatus.REVOKED]]
        finished_job_ids = [str(job.uuid) for job in finished_jobs]
        try:
            base_dir = '/scratch'
            # Prune all job scratch directories older than the time threshold
            time_threshold = datetime.now(timezone.utc) - timedelta(seconds=settings.CE_JOB_EXPIRATION_PERIOD)
            logger.debug(f'''Scratch folders older than {time_threshold.strftime('%Y/%m/%d %H:%M:%S')} '''
                         '''will be deleted.''')
            scratch_job_ids = []
            uuid_regex = r'^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$'
            for job_id in os.listdir(base_dir):
                scratch_dir = os.path.join(base_dir, job_id)
                # Must be a directory and valid UUID
                if not os.path.isdir(scratch_dir) or not bool(re.match(uuid_regex, job_id)):
                    continue
                # Collect list of job IDs
                scratch_job_ids.append(job_id)
                file_stat = os.stat(scratch_dir)
                created_time = datetime.fromtimestamp(file_stat.st_ctime, tz=timezone.utc)
                if created_time < time_threshold:
                    delete_job_scratch_files(job_id)
            # Prune all job scratch directories for non-active jobs
            logger.debug('''Scratch folders from finished or missing/deleted jobs will be deleted.''')
            # Delete all scratch directories associated with deleted/untracked jobs
            for job_id in [job_id for job_id in scratch_job_ids if job_id not in all_job_ids]:
                logger.info(f'''Pruning orphaned job scratch files for missing/deleted job {job_id}''')
                delete_job_scratch_files(job_id)
            # Delete all scratch directories for finished jobs
            for job_id in [job_id for job_id in scratch_job_ids if job_id in finished_job_ids]:
                logger.info(f'''Pruning orphaned job scratch files for finished job {job_id}''')
                delete_job_scratch_files(job_id)
        except Exception as err:
            logger.error(f'''Error pruning orphaned job scratch files: {err}''')

        try:
            # Collect all job directories in S3 bucket
            s3 = ObjectStore()
            s3_basepath = os.path.join(settings.S3_BASE_DIR, 'jobs')
            s3_job_ids = []
            paths = s3.list_directory(s3_basepath, recursive=True)
            for path in paths:
                job_id = path.replace(s3_basepath, '', 1)
                job_id = str(Path(job_id).parts[1])
                if not bool(re.match(uuid_regex, job_id)):
                    continue
                if job_id not in s3_job_ids:
                    s3_job_ids.append(job_id)
            # Delete all orphaned job directories from the bucket
            if s3_job_ids:
                logger.error('''Pruning orphaned job files in S3 bucket...''')
                orphaned_job_ids = [job_id for job_id in s3_job_ids if job_id not in all_job_ids]
                for orphaned_job_id in orphaned_job_ids:
                    logger.info(f'''Pruning orphaned job files for job {orphaned_job_id}''')
                    delete_job_files(orphaned_job_id)
        except Exception as err:
            logger.error(f'''Error pruning orphaned job files in S3 bucket: {err}''')


class PruneJobs():

    @property
    def task_name(self):
        return "Prune jobs"

    @property
    def task_handle(self):
        return self.task_func

    @property
    def task_frequency_seconds(self):
        return settings.CE_JOB_PURGE_INTERVAL

    @property
    def task_initially_enabled(self):
        return True

    def __init__(self, task_func='') -> None:
        self.task_func = task_func

    def run_task(self):
        logger.info(f'Running periodic task "{self.task_name}"...')

        time_threshold = datetime.now(timezone.utc) - timedelta(seconds=settings.CE_JOB_EXPIRATION_PERIOD)
        logger.debug(f'''Jobs older than {time_threshold.strftime('%Y/%m/%d %H:%M:%S')} will be deleted.''')
        # Delete jobs older than the expiration period regardless of status
        expired_jobs = Job.objects.filter(saved__exact=False,
                                          created__lt=time_threshold,
                                          # status__in=[Job.JobStatus.SUCCESS,
                                          #             Job.JobStatus.FAILURE,
                                          #             Job.JobStatus.REVOKED],
                                          )
        logger.info(f'Number of expired jobs scheduled for deletion: {len(expired_jobs)}.')
        # TODO: Need to find and delete orphaned job data
        for job in expired_jobs:
            job_id = str(job.uuid)
            # Delete job files
            delete_job_files.delay(job_id)
            # Delete job records from database
            job.delete()
            logger.info(f'Deleted expired job "{job_id}".')


class CollectMetrics():

    @property
    def task_name(self):
        return "Collect metrics"

    @property
    def task_handle(self):
        return self.task_func

    @property
    def task_frequency_seconds(self):
        return settings.CE_COLLECT_METRICS_INTERVAL

    @property
    def task_initially_enabled(self):
        return True

    def __init__(self, task_func='') -> None:
        self.task_func = task_func

    def run_task(self):
        logger.info(f'Running periodic task "{self.task_name}"...')
        # Query database for jobs created in the last hour
        # TODO: Is it acceptable that this is not guaranteed to be accurate, that it is an undercount?
        time_threshold = datetime.now(timezone.utc) - timedelta(seconds=settings.CE_COLLECT_METRICS_INTERVAL)
        logger.debug(f'''Collecting metrics since {time_threshold.strftime('%Y/%m/%d %H:%M:%S')}...''')
        # Count number of recent jobs
        recent_jobs = Job.objects.filter(created__gt=time_threshold)
        # Count the number of times a module is used across all jobs
        module_usage = {}
        for job in recent_jobs:
            # Modules can be invoked multiple times in a single workflow,
            # so count each instance.
            wf_config = job.config['workflow']['config']
            for process in wf_config['processes']:
                module_name = process['module']
                if module_name in module_usage:
                    module_usage[module_name] += 1
                else:
                    module_usage[module_name] = 1
        # Count total number of users
        all_users = User.objects.filter()
        all_uploads = Upload.objects.filter()
        all_job_files = JobFile.objects.filter()
        # Count number of unique users who ran jobs
        job_owners = []
        for job in recent_jobs:
            if job.owner not in job_owners:
                job_owners.append(job.owner)
        Metric.objects.create(
            jobs_total=len(recent_jobs),
            jobs_success=len([job for job in recent_jobs if job.status == Job.JobStatus.SUCCESS]),
            jobs_failure=len([job for job in recent_jobs if job.status == Job.JobStatus.FAILURE]),
            users_count=len(all_users),
            users_active=len(job_owners),
            uploads_total=len(all_uploads),
            uploads_size=sum([upload.size for upload in all_uploads]),
            job_files_total=len(all_job_files),
            job_files_size=sum([job_file.size for job_file in all_job_files]),
            module_usage=module_usage,
        )


@shared_task
def prune_orphaned_data():
    PruneOrphanedData().run_task()


@shared_task
def prune_jobs():
    PruneJobs().run_task()


@shared_task
def collect_metrics():
    CollectMetrics().run_task()


periodic_tasks = [
    PruneOrphanedData(task_func='prune_orphaned_data'),
    PruneJobs(task_func='prune_jobs'),
    CollectMetrics(task_func='collect_metrics'),
]
