from calculation_engine.models import Job, Upload
from calculation_engine.log import get_logger
from django.conf import settings
from calculation_engine.config import load_config_from_file
from django.db.models import Q
logger = get_logger(__name__)


def get_usage_limits(user=None):
    if user:
        # Apply the highest tier group in which the user is a member
        logger.debug(f'User {user.username} groups: {[group.name for group in user.groups.all()]}')
        for idx, tier_name in enumerate(settings.USAGE_LIMIT_TIERS):
            logger.debug(f'{idx}: {tier_name}')
            usage_limit_tier_idx = idx
            usage_limit_tier = [group.name for group in user.groups.all() if group.name == tier_name]
            logger.debug(f'  {usage_limit_tier}')
            # Assume ordering of USAGE_LIMIT_TIERS is least to most restrictive. Break upon first match.
            if usage_limit_tier:
                usage_limit_tier = usage_limit_tier[0]
                break
        # If the user is not in a tier group, apply the lowest tier
        if not usage_limit_tier:
            usage_limit_tier_idx = len(settings.USAGE_LIMIT_TIERS) - 1
            usage_limit_tier = settings.USAGE_LIMIT_TIERS[len(settings.USAGE_LIMIT_TIERS) - 1]
        # If the user is an admin, apply the highest tier
        if user.is_superuser:
            usage_limit_tier_idx = 0
            usage_limit_tier = settings.USAGE_LIMIT_TIERS[0]
    else:
        # If no user is specified, apply the lowest tier
        usage_limit_tier_idx = len(settings.USAGE_LIMIT_TIERS) - 1
        usage_limit_tier = settings.USAGE_LIMIT_TIERS[len(settings.USAGE_LIMIT_TIERS) - 1]
    config = load_config_from_file()
    limits = config['limits']
    for usage_limit_label in config['limits']:
        # Select the maximum allowed limit
        idx = 0
        while idx <= usage_limit_tier_idx:
            try:
                usage_limit = limits[usage_limit_label]['limits'][usage_limit_tier_idx]
                idx += 1
            except IndexError:
                break
        limits[usage_limit_label]['limit'] = usage_limit
        del limits[usage_limit_label]['limits']
        logger.debug(f'''limits: {limits}''')
    return limits


def usage_limit_exceeded(label='', user=None):
    assert label
    assert user
    msg = ''
    limits = get_usage_limits(user=user)
    if label == 'all' or label == 'saved_jobs':
        actual_value = len(Job.objects.filter(owner__exact=user, saved__exact=True))
        limit_value = limits[label]['limit']
        logger.debug(f'''Saved jobs: {actual_value} / {limit_value}''')
        if actual_value >= limit_value:
            msg = ('You have reached your limit for the maximum number '
                   f'of saved jobs: {limit_value}')
            return True, msg
    if label == 'all' or label == 'upload_size':
        uploads = Upload.objects.filter(owner__exact=user)
        # total upload size in bytes
        actual_value = sum([upload.size for upload in uploads])
        # total upload size in gibibytes
        actual_value_GiB = round(actual_value / 1024**3, 2)
        limit_value_GiB = limits[label]['limit']
        limit_value = limit_value_GiB * 1024**3
        limit_value_GB = round(limit_value / 1000**3, 2)
        logger.debug(
            f'''actual_value_GiB: {actual_value_GiB}\n'''
            f'''limit_value_GiB: {limit_value_GiB}\n'''
            f'''limit_value: {limit_value}\n'''
            f'''limit_value_GB: {limit_value_GB}\n'''
        )
        if actual_value >= limit_value:
            msg = ('You have reached your limit for the total size '
                   f'of uploaded files: {limit_value} GiB ({limit_value_GB} GB).'
                   f'Current total size is: {actual_value} GiB')
            return True, msg
    if label == 'all' or label == 'concurrent_jobs':
        actual_value = Job.objects.filter(
            Q(owner__exact=user) &  # noqa W504
            ~Q(status__in=[Job.JobStatus.SUCCESS,
                           Job.JobStatus.FAILURE,
                           Job.JobStatus.REVOKED])
        )
        limit_value = limits[label]['limit']
        if len(actual_value) >= limit_value:
            msg = ('You have reached your limit for the maximum number '
                   f'of concurrent active jobs: {limit_value}')
            return True, msg
    return False, msg
