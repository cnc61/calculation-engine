from pathlib import Path
import logging.config
import os
from corsheaders.defaults import default_headers

django_base_dir = Path(__file__).resolve().parent.parent
# sys.path.append(str(django_base_dir))

CE_BASE_DIR = os.environ.get('CE_BASE_DIR', '/opt')
assert os.path.isabs(CE_BASE_DIR)
S3_BASE_DIR = os.environ.get('S3_BASE_DIR', '').strip('/')
CE_MODULES_DIR = os.path.join(CE_BASE_DIR, 'modules')
CE_CONFIG_DIR = os.path.join(CE_BASE_DIR, 'config')
CE_CONFIG_PATH = os.path.join(CE_CONFIG_DIR, 'config.yaml')
CE_CONFIG_SPEC_PATH = os.path.join(CE_CONFIG_DIR, 'config.openapi.yaml')
CE_CONFIG_PROCESSED_PATH = os.path.join(CE_CONFIG_DIR, 'config.processed.yaml')
CE_CONFIG_SECRET_PATH = os.path.join(CE_CONFIG_DIR, 'secret.yaml')
CE_JOB_EXPIRATION_PERIOD = int(os.getenv('CE_JOB_EXPIRATION_PERIOD', 48 * 3600))
CE_JOB_PURGE_INTERVAL = int(os.getenv('CE_JOB_PURGE_INTERVAL', 3600))
CE_COLLECT_METRICS_INTERVAL = int(os.getenv('CE_COLLECT_METRICS_INTERVAL', 3600))
CE_JOB_SCRATCH_FREE_SPACE = int(os.getenv('CE_JOB_SCRATCH_FREE_SPACE', 1024 * 1024**2))
# CE_JOB_SCRATCH_FREE_PERCENT = int(os.getenv('CE_JOB_SCRATCH_FREE_PERCENT', 10))
# assert CE_JOB_SCRATCH_FREE_PERCENT in range(0, 101)

SHARED_HASH_SALT = os.getenv('HASH_SALT', '')

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('DJANGO_SECRET_KEY', 'django-dummy-secret')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ.get("DJANGO_DEBUG", "false").lower() == "true"
# SILKY_PYTHON_PROFILER = (os.environ.get("SILKY_PYTHON_PROFILER", "false").lower() == "true")

#
# Cross Site Request Forgery protection
# https://docs.djangoproject.com/en/4.2/ref/csrf/
#
# API_SERVER_HOST is the internal service name
API_SERVER_HOST = os.environ.get("API_SERVER_HOST", "api-server")
# API_SERVER_PORT is the port associated with the Django webserver itself
API_SERVER_PORT = os.environ.get("API_SERVER_PORT", "8000")
# API_PROXY_PORT is the port associated with the proxy webserver
API_PROXY_PORT = os.environ.get("API_PROXY_PORT", "4000")
# HOSTNAMES is the list of domains associated with the ingress
HOSTNAMES = os.environ.get("DJANGO_HOSTNAMES", "localhost").split(",")
for hostname in ['localhost', '127.0.0.1', 'api-proxy']:
    if hostname not in HOSTNAMES:
        HOSTNAMES.append(hostname)
ALLOWED_HOSTS = HOSTNAMES
if API_SERVER_HOST:
    ALLOWED_HOSTS.append(API_SERVER_HOST)
CSRF_TRUSTED_ORIGINS = [f"http://localhost:{API_PROXY_PORT}", f"http://localhost:{API_SERVER_PORT}"]
for hostname in ALLOWED_HOSTS:
    CSRF_TRUSTED_ORIGINS.append(f'''http://{hostname}''')
    CSRF_TRUSTED_ORIGINS.append(f'''https://{hostname}''')
CSRF_COOKIE_SECURE = True
#
# CORS configuration
# https://pypi.org/project/django-cors-headers/
#
CORS_ALLOW_ALL_ORIGINS = False
CORS_ALLOWED_ORIGINS = CSRF_TRUSTED_ORIGINS
# CORS_ALLOWED_ORIGINS = ["*"]
CORS_ALLOW_CREDENTIALS = True
CORS_ALLOW_HEADERS = list(default_headers) + [
    # add custom headers here
]

# Application definition

SITE_ID = 1

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_filters',
    'django_bootstrap5',
    'corsheaders',
    'rest_framework',
    'rest_framework.authtoken',
    'mozilla_django_oidc',
    'django_celery_results',
    'django_celery_beat',
    # 'silk',
    'storages',
    'calculation_engine',
    'users',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'silk.middleware.SilkyMiddleware',
]

ROOT_URLCONF = 'app_base.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(django_base_dir, 'app_base/templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

ASGI_APPLICATION = 'app_base.asgi.application'

# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': os.getenv('DB_ENGINE', 'django.db.backends.postgresql'),
        'NAME': os.getenv('POSTGRES_DB', 'postgres'),
        'USER': os.getenv('POSTGRES_USER', 'postgres'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD', 'postgres'),
        'HOST': os.getenv('DATABASE_HOST', '127.0.0.1'),
        'PORT': os.getenv('DATABASE_PORT', '5432'),
    },
    'sqlite': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(CE_BASE_DIR, 'db.sqlite3'),
    },
}

# Default primary key field type
# https://docs.djangoproject.com/en/4.0/ref/settings/#default-auto-field
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'app_base.auth_backends.KeycloakOIDCAuthenticationBackend',
)

LOGIN_URL = '/oidc/authenticate'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = os.environ.get('OIDC_OP_LOGOUT_ENDPOINT', '/')
# Our django backend is deployed behind nginx/guncorn. By default Django ignores
# the X-FORWARDED request headers generated. mozilla-django-oidc calls
# Django's request.build_absolute_uri method in such a way that the https
# request produces an http redirect_uri. So, we need to tell Django not to ignore
# the X-FORWARDED header and the protocol to use:
USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')


# Configure OIDC client
OIDC_RP_CLIENT_ID = os.environ.get('OIDC_CLIENT_ID', '')
OIDC_RP_CLIENT_SECRET = os.environ.get('OIDC_CLIENT_SECRET', '')
OIDC_RP_SCOPES = os.environ.get('OIDC_SCOPES', '')
if not OIDC_RP_SCOPES:
    OIDC_RP_SCOPES = "openid profile email"
OIDC_OP_AUTHORIZATION_ENDPOINT = os.environ.get('OIDC_OP_AUTHORIZATION_ENDPOINT', '')
OIDC_OP_TOKEN_ENDPOINT = os.environ.get('OIDC_OP_TOKEN_ENDPOINT', '')
OIDC_OP_USER_ENDPOINT = os.environ.get('OIDC_OP_USER_ENDPOINT', '')
# OIDC_AUTHENTICATION_CALLBACK_URL=os.environ.get('OIDC_AUTHENTICATION_CALLBACK_URL', 'oidc_authentication_callback')

STAFF_GROUP = os.environ.get('STAFF_GROUP', '/ce')
ADMIN_GROUP = os.environ.get('ADMIN_GROUP', '/ce/admin')
# Order by decreasing capability: the first item is the least restricted.
USAGE_LIMIT_TIERS = [
    'tier2',
    'tier1',
    'tier0',
]
GROUP_MAP = {
    # The default group is in usage limit tier0
    '/ce': USAGE_LIMIT_TIERS[2],
    '/ce/tier1': USAGE_LIMIT_TIERS[1],
    # Admins are implicitly in usage limit tier2
    '/ce/admin': USAGE_LIMIT_TIERS[0],
    '/ce/tier2': USAGE_LIMIT_TIERS[0],
}

# Required for Keycloak
OIDC_RP_SIGN_ALGO = os.environ.get('OIDC_RP_SIGN_ALGO', 'RS256')
OIDC_OP_JWKS_ENDPOINT = os.environ.get('OIDC_OP_JWKS_ENDPOINT', '')
# this method is invoke upon /logout -> mozilla_django_oidc.ODICLogoutView.post
OIDC_OP_LOGOUT_URL_METHOD = 'app_base.auth_backends.execute_logout'

# OIDC_USERNAME_ALGO
# ref: https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html#generating-usernames
OIDC_USERNAME_ALGO = 'app_base.auth_backends.generate_username'

# ALLOW_LOGOUT_GET_METHOD tells mozilla-django-oidc that the front end can logout with a GET
# which allows the front end to use location.href to /auth/logout to logout.
ALLOW_LOGOUT_GET_METHOD = True

# SESSION_ENGINE
# ref: https://github.com/mozilla/mozilla-django-oidc/issues/435#issuecomment-1036372844
# ref: https://docs.djangoproject.com/en/4.0/topics/http/sessions/
SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"

# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = False

USE_TZ = True

DATETIME_FORMAT = 'Y-m-d H:m:s'
DATE_FORMAT = 'Y-m-d'


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.0/howto/static-files/

STATIC_URL = '/static/'
# STATIC_ROOT tells collectstatic where to copy all the static files that it collects.
STATIC_ROOT = os.path.join(CE_BASE_DIR, 'static')

MEDIA_URL = '/uploads/'
MEDIA_ROOT = os.path.join(CE_BASE_DIR, 'uploads')

STORAGES = {
    "default": {
        "BACKEND": "storages.backends.s3.S3Storage",
        # https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html
        "OPTIONS": {
            # 'url_protocol': os.getenv('AWS_S3_URL_PROTOCOL', 'http:'),
            'bucket_name': os.getenv('S3_BUCKET', 'calculation-engine'),
            'location': os.getenv('AWS_LOCATION', 'uploads'),
            'endpoint_url': os.getenv('S3_ENDPOINT_URL', ''),
            'region_name': os.getenv('AWS_S3_REGION_NAME', ''),
            'access_key': os.getenv('AWS_S3_ACCESS_KEY_ID', ''),
            'secret_key': os.getenv('AWS_S3_SECRET_ACCESS_KEY', ''),
        },
    },
    "staticfiles": {
        "BACKEND": "django.contrib.staticfiles.storage.StaticFilesStorage",
    },
}

# Celery configuration
# ref: https://docs.celeryq.dev/en/stable/django/first-steps-with-django.html#django-celery-results-using-the-django-orm-cache-as-a-result-backend

CELERY_RESULT_BACKEND = 'django-db'
CELERY_CACHE_BACKEND = 'default'

CELERY_BEAT_SCHEDULER = "django_celery_beat.schedulers:DatabaseScheduler"
CELERY_TIMEZONE = "UTC"

CELERY_IMPORTS = [
    "calculation_engine.tasks",
    "calculation_engine.tasks_api",
    "calculation_engine.system_tasks",
]

CELERY_TASK_ROUTES = {
    'Revoke Job': {'queue': 'api'},
    'Delete Job Files': {'queue': 'api'},
    'Delete Job': {'queue': 'api'},
    'calculation_engine.system_tasks.*': {'queue': 'api'},
}

# Caching
# https://docs.djangoproject.com/en/dev/topics/cache/#filesystem-caching

REDIS_SERVICE = os.environ.get('REDIS_SERVICE', 'redis')
CACHES = {
    'default': {
        "BACKEND": "django.core.cache.backends.redis.RedisCache",
        "LOCATION": f"redis://{REDIS_SERVICE}:6379",
    }
}

API_RATE_LIMIT_ANON = int(os.getenv('API_RATE_LIMIT_ANON', '30'))
API_RATE_LIMIT_USER = int(os.getenv('API_RATE_LIMIT_USER', '2'))
API_RATE_LIMIT_DOWNLOAD = int(os.getenv('API_RATE_LIMIT_DOWNLOAD', '10'))

REST_FRAMEWORK = {
    'DEFAULT_METADATA_CLASS': 'rest_framework.metadata.SimpleMetadata',
    'DEFAULT_PERMISSION_CLASSES': ['rest_framework.permissions.AllowAny'],
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 100,
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ],
    'DEFAULT_THROTTLE_CLASSES': [
        'rest_framework.throttling.AnonRateThrottle',
        'rest_framework.throttling.UserRateThrottle',
        'rest_framework.throttling.ScopedRateThrottle',
    ],
    'DEFAULT_THROTTLE_RATES': {
        'anon': f'''{API_RATE_LIMIT_ANON}/minute''',
        'user': f'''{API_RATE_LIMIT_USER}/second''',
        'download': f'''{API_RATE_LIMIT_DOWNLOAD}/second''',
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        }
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'INFO'
        },
        'mozilla_django_oidc': {
            'handlers': ['console'],
            'level': 'INFO'
        },
    }
}
logging.config.dictConfig(LOGGING)

# logging.info(f'''settings.py final configuration:\n{yaml.dump(config)}''')
