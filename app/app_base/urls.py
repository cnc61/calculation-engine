from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from app_base import views
from calculation_engine import urls as ce_urls
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view
from calculation_engine.config import load_config_from_file

# # Imports for serving static files in development mode
# # ref: https://docs.djangoproject.com/en/4.2/howto/static-files/#serving-static-files-during-development
# from django.conf import settings
# from django.conf.urls.static import static

from calculation_engine.log import get_logger
logger = get_logger(__name__)


router = routers.DefaultRouter()
router.register(r'user', views.UserViewSet)

urlpatterns = [
    path('', views.HomePageView, name='home'),
    path('token', views.CustomAuthToken.as_view(), name='token'),
    path('request-access', views.RequestAccess.as_view(), name='request-access'),
    path('admin/', admin.site.urls, name='admin'),
    path('oidc/', include('mozilla_django_oidc.urls')),
    path('users/', views.UserListView.as_view(), name='user-list-page'),
    path('', include('users.urls')),
    path('api/v0/', include(router.urls), name='base-api'),
    path('api/v0/token/', views.get_token, name='token-api'),
    path('ce/', include(ce_urls.page_urlpatterns)),
    path('api/v0/ce/', include(ce_urls.api_urlpatterns)),
]
# ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

config = load_config_from_file()
urlpatterns += [
    # Use the `get_schema_view()` helper to add a `SchemaView` to project URLs.
    #   * `title` and `description` parameters are passed to `SchemaGenerator`.
    #   * Provide view name for use with `reverse()`.
    path('openapi/', get_schema_view(
        title="MUSES Calculation Engine API",
        description="API for the MUSES Calculation Engine",
        version=config['version'],
        patterns=urlpatterns,
        public=True,
        # urlconf='app_base.urls',
    ), name='openapi-schema'),
    # ...
    # Route TemplateView to serve Swagger UI template.
    #   * Provide `extra_context` with view name of `SchemaView`.
    path('swagger-ui/', TemplateView.as_view(
        template_name='calculation_engine/swagger-ui.html',
        extra_context={'schema_url': 'openapi-schema'}
    ), name='swagger-ui'),
]

# Django Silk profiler (https://github.com/jazzband/django-silk)
# urlpatterns += [path('silk/', include('silk.urls', namespace='silk'))]
