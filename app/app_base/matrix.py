import os
import requests
import bcrypt
from urllib import parse
from calculation_engine.log import get_logger
from django.conf import settings
logger = get_logger(__name__)


class SynapseApi():
    """
    https://github.com/matrix-org/synapse/blob/master/docs/admin_api/
    """
    def __init__(self, conf={}):
        # Set default config
        self.conf = {
            # Synapse server URL (e.g. https://matrix.musesframework.io)
            'server_url': os.environ.get('MATRIX_HOMESERVER_URL', 'https://matrix.musesframework.io'),
            # Bot account access token
            'access_token': os.environ.get('MATRIX_ACCESS_TOKEN', ''),
            # Target Matrix room ID
            'room_id': os.environ.get('MATRIX_ROOM_ID', ''),
        }
        # Override config parameters from input
        for param in self.conf:
            if param in conf:
                self.conf = conf[param]

    def send_message(self, message_body=None, message_body_html=None, room_id=None):
        try:
            if not room_id:
                room_id = self.conf['room_id']
            assert message_body and room_id
            json = {
                "msgtype": "m.text",
                "body": message_body,
            }
            # If a formatted message body has been provided, for example if a markdown-formatted
            # text has been rendered as HTML, include it in the message event
            # ref: https://spec.matrix.org/v1.2/client-server-api/#mroommessage-msgtypes
            if message_body_html:
                json['format'] = "org.matrix.custom.html"
                json['formatted_body'] = message_body_html
            response = requests.post(
                f'''{self.conf['server_url']}/_matrix/client/r0/rooms/{room_id}/send/m.room.message''',
                params={
                    "access_token": self.conf['access_token'],
                },
                json=json
            )
            try:
                assert response.status_code in [200, 204]
                return True
            except Exception as err:
                logger.error(f'''{err}''')
                logger.error(f'''Error sending message to room "{room_id}": [{response.status_code}] {response.text}''')
                return False
        except Exception as err:
            logger.error(f'''Error sending message to room "{room_id}": {err}''')
            return False


def notify_new_user(user_info):
    api = SynapseApi()
    email = user_info['email']
    username = user_info['username']

    message_body = f'''New CE user: "{user_info['first_name']} {user_info['last_name']}", {username}, {email}\n\n'''

    message_body_html = f'''
        <h2>New CE user access request</h2>
        <p>
            name: {user_info['first_name']} {user_info['last_name']}<br />
            username: <code>{username}</code><br />
            email: <code>{email}</code>
        </p>
        <p>
            Approval will add the user to the "ce" Keycloak group and the 
            <a href="https://forum.musesframework.io/g/ce-users">"ce-users" Discourse group</a>
            (if they are not a collaborator).
        </p>
    '''

    # Generate hash of email address using secret salt
    email_hash = bcrypt.hashpw(bytes(email, 'utf-8'), bytes(settings.SHARED_HASH_SALT, "utf-8"))

    email_hash_urlencoded = parse.quote_plus(email_hash)
    email_urlencoded = parse.quote_plus(email)

    approve_link = f'''https://musesframework.io/api/v1/register/ce/approve/{email_urlencoded}?token={email_hash_urlencoded}'''
    deny_link = f'''https://musesframework.io/api/v1/register/ce/reject/{email_urlencoded}?token={email_hash_urlencoded}'''

    message_body += f'''[✅ Click here to APPROVE the request]({approve_link})\n\n'''
    message_body += f'''[🚫 Click here to DENY the request]({deny_link})\n\n'''

    message_body_html += f'''
        <hr>
            <p><a href="{approve_link}">✅ Click here to APPROVE the request.</a></p>
        <hr>
            <p><a href="{deny_link}">🚫 Click here to DENY the request.</a></p>
        <hr>
    '''

    # logger.debug(f'''{message_body}''')
    logger.debug(f'''{message_body_html}''')

    api.send_message(message_body=message_body, message_body_html=message_body_html)
