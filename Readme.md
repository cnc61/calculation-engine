# MUSES Calculation Engine

## Overview

The [MUSES project](https://musesframework.io) is an NSF-funded large collaboration project tasked with developing new cyberinfrastructure to provide the scientific community novel tools to answer critical interdisciplinary questions in nuclear physics, gravitational wave astrophysics, and heavy-ion physics. It has three major software development goals: (1) create independent software modules that calculate EoSs, calculate physical observables, and/or process data; (2) define a software framework for integration and interoperability of these modules (and external tables); and (3) design and construct a management system to orchestrate the execution of composable, multi-module workflows.

The MUSES Calculation Engine lies at the intersection of these goals; it is a web application and workflow management system that orchestrates the execution of a set of MUSES modules according to an arbitrary directed acyclic graph (DAG) specified by the user. Each execution of a workflow is a job managed via the RESTful API. Jobs are added to a processing queue and run asynchronously across a pool of worker processes, efficiently leveraging parallelism within the constraints of the workflow's DAG to optimally utilize computing resources. Data can be piped between modules or input from uploaded files, and all generated output files can be downloaded from an S3-compatible file repository for local analysis.

## Components

![](app/assets/img/Calculation_Engine_architecture.drawio.png)

The primary system components are

* RESTful HTTP API server
* SQL database
* Task queue system
* Object store

### RESTful HTTP API server

The API server is a Django application relying heavily on the Django REST Framework. It is the primary interface with which users interact, either via Jupyter notebooks containing a Python library for convenience, or directly through HTTP requests to API endpoints.

### Load balancer

To support running multiple replicas of the API server, a load balancer is deployed to proxy HTTP requests in round-robin style to the Django instances. In local development this loadbalancer is an NGINX webserver instance. In Kubernetes, this is MetalLB and the Traefik ingress controller.

### PostgreSQL database

The official PostgreSQL database is deployed to provide operational storage for the Django ORM. It is also configured as the Celery result backend via the `django-celery-results` extension.

### Object storage and web proxy

An instance of a MinIO server provides S3-compatible object storage for job results. [MinIO includes a web interface](http://localhost:9001/browser) for browsing bucket contents and configuring the instance. There is also an instance of nginx-s3-gateway to provide a standard HTTP interface to the S3 object store.

### Task queueing system: Celery + RabbitMQ

Celery is "a task queue with focus on real-time processing, while also supporting task scheduling." A set of Celery worker replicas communicate via the RabbitMQ message broker to execute the processing tasks launched by the API server in response to user requests.

### Observation and monitoring tools

[RabbitMQ provides a web interface](http://127.0.0.1:15672/#/) to monitor message queues and broker nodes. Celery offers a web interface called [Flower](http://localhost:5556/tasks) to monitor task queues and status.

## Compile Sphinx-compatible module documentation

See [docs/Readme.md](docs/Readme.md) for instructions about how to compile the MUSES documentation.

## Developer documentation

See [the developer docs](docs/src/developer/calculation-engine.md) for information about building and running the Calculation Engine in Docker Compose.
