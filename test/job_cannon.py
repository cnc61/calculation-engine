import os
import sys
from pathlib import Path
import time
from multiprocessing import Process
from datetime import datetime
import yaml

# Append the calculation_engine_api module to the Python path for import
sys.path.append(os.path.join(str(Path(__file__).resolve().parent.parent), 'app'))
from calculation_engine.tests.calculation_engine_api import CalculationEngineApi
from calculation_engine.tests.test_workflows import WorkflowTests


'''
Usage:
```bash
    source .venv/bin/activate
    # Run one or more tests by name
    python test/job_cannon.py 1 cmf chiral_eft
    # Run all tests
    python test/job_cannon.py 1 all
```
'''


class JobCannon():
    def __init__(self) -> None:
        self.api = CalculationEngineApi()
        self.tester = WorkflowTests()
        self.tester.apis = {'cannon': self.api}


def job_delete_all():
    cannon = JobCannon()
    api = cannon.api
    jobs = api.job_list()
    for job in jobs:
        job_id = job['uuid']
        print(f'Deleting job "{job_id}"...')
        api.job_delete(uuid=job_id)


def list_jobs():
    cannon = JobCannon()
    api = cannon.api
    jobs = api.job_list()
    if not jobs:
        return
    job_info = []
    for job in jobs:
        # job.pop('config')
        job_info.append(job)
    # jobs = [job.pop('config') for job in jobs]
    print(f'''{len(jobs)} jobs total''')
    print(f'''{len([job for job in job_info if job['status'] == 'FAILURE'])} failed jobs''')
    print(f'''{len([job for job in job_info if job['status'] == 'SUCCESS'])} successful jobs''')
    print(f'''{len([job for job in job_info if job['status'] in ['PENDING', 'STARTED']])} incomplete jobs''')
    with open('job_info.yaml', 'w') as fp:
        yaml.dump(sorted(job_info, key=lambda jb: jb['created']), fp)
    print(f'''created  first: {sorted([job['created'] for job in job_info])[0]}''')
    print(f'''created  last:  {sorted([job['created'] for job in job_info])[-1]}''')
    print(f'''modified first: {sorted([job['modified'] for job in job_info])[0]}''')
    print(f'''modified last:  {sorted([job['modified'] for job in job_info])[-1]}''')


def run_test():
    cannon = JobCannon()
    api = cannon.api
    wf_config_yaml = '''
        processes:
        - name: test
          module: test_vehicle
          config: {}
        components:
        - type: group
          name: run_test
          group:
          - test
    '''
    wf_config = yaml.safe_load(wf_config_yaml)
    response = api.job_create(config={
        "workflow": {
            "config": wf_config
        }
    })
    job_id = response['uuid']
    time_sleep = 5  # seconds
    time_start = time.time()
    response = api.job_list(uuid=job_id)
    while 60 > time.time() - time_start and response['status'] not in ['SUCCESS', 'FAILURE']:
        time.sleep(time_sleep)
        response = api.job_list(uuid=job_id)
    print(response['status'])
    assert response['status'] == 'SUCCESS'


def launch_jobs(num_cycles=1, workflows=[]):
    cannon = JobCannon()

    workflow_tests = {
        # 'test_vehicle': run_test,
        'crust_dft': cannon.tester.test_crust_dft,
        'crust_dft_lepton': cannon.tester.test_crust_dft_lepton,
        'crust_dft_cmf_lepton_synthesis_qlimr': cannon.tester.test_crust_dft_cmf_lepton_synthesis_qlimr,
        'cmf': cannon.tester.test_cmf,
        'eos_taylor_4d': cannon.tester.test_eos_taylor_4d,
        'eos_ising_texs_2d': cannon.tester.test_eos_ising_texs_2d,
        'lepton': cannon.tester.test_lepton,
        'holographic_eos': cannon.tester.test_holographic_eos,
        'flavor_equilibration': cannon.tester.test_flavor_equilibration,
        'chiral_eft': cannon.tester.test_chiral_eft,
        'cmf_lepton': cannon.tester.test_cmf_lepton,
        'chiral_eft_lepton': cannon.tester.test_chiral_eft_lepton,
        'cmf_lepton_synthesis_qlimr': cannon.tester.test_cmf_lepton_synthesis_qlimr,
    }
    procs = []
    if 'all' in workflows:
        workflow_set = [workflow_tests[wf_name] for wf_name in workflow_tests]
    else:
        workflow_set = [workflow_tests[wf_name] for wf_name in workflow_tests if wf_name in workflows]
    for cycle in range(1, num_cycles + 1):
        for workflow in workflow_set:
            proc = Process(target=workflow, name=f'''{workflow.__name__}-{cycle}''')
            proc.start()
            # proc.join()
            procs.append(proc)
            print(f'[{datetime.now().strftime("%H:%M:%S")}] Process started: {proc.name}''')
    running_procs = ['init']
    finished_procs = []
    while procs:
        time.sleep(5)
        running_procs = []
        for proc in procs:
            if proc.is_alive():
                running_procs.append(proc)
            else:
                finished_procs.append(proc)
                print(f'[{datetime.now().strftime("%H:%M:%S")}] Process finished: {proc.name}''')
                if proc.exitcode:
                    print(f'[{datetime.now().strftime("%H:%M:%S")}] PROCESS ERROR: {proc}''')
                # print(f'[{datetime.now().strftime("%H:%M:%S")}] Process running: {proc.name}''')
        procs = running_procs
    for proc in finished_procs:
        if proc.exitcode:
            print(f'[{datetime.now().strftime("%H:%M:%S")}] PROCESS ERROR: {proc}''')


if __name__ == "__main__":
    num_cycles = 1
    if len(sys.argv) > 1:
        num_cycles = int(sys.argv[1])
    if len(sys.argv) > 2:
        workflows = sys.argv[2:]
    launch_jobs(num_cycles=num_cycles, workflows=workflows)
