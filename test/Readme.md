This folder contains (typically ephemeral) scripts used by developers for collaborative purposes.

Scripts that test application and component functionality, such as unit tests and processing 
workflows, should reside within `/app/calculation_engine/tests` compatible with the Django test
framework.
